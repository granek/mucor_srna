This repository contains the custom software developed to analyze the high-throughput sRNA data for [Antifungal drug resistance evoked via RNAi-dependent epimutations](http://www.ncbi.nlm.nih.gov/pubmed/25079329).  The [raw data](http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE56353) from this work is available from [GEO](http://www.ncbi.nlm.nih.gov/geo/).  To reproduce this analysis:

1.  Download this repository 
    
    -   Using git: `git clone https://bitbucket.org/granek/mucor_srna.git`
    
    -   Download a zip file: <https://bitbucket.org/granek/mucor_srna/downloads>

2.  Install the prerequisites detailed below.

3.  Move into the repository directory: `cd mucor_srna`

4.  Run the analysis: `make`


# Prerequisites

## Software

The versions given below have been tested with this repository, and are known to work, but earlier versions may work perfectly well

-   [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) (version 2.2.3)

-   [TopHat](http://ccb.jhu.edu/software/tophat/index.shtml) (version 2.0.12)

-   [FASTX-Toolkit](http://hannonlab.cshl.edu/fastx_toolkit/download.html) (version 0.0.13)

-   [NCBI SRA Toolkit](http://www.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?view=software) (version 2.3.5)

-   Likely to be installed on most Linux servers

    -   [Python 2.7](https://www.python.org/download/releases/2.7.8/)
    
    -   [LaTeX](http://www.ctan.org/starter) 
        
        -   [texshade](http://www.ctan.org/tex-archive/macros/latex/contrib/texshade) will automatically be installed when make is run
    
    -   [wget](https://www.gnu.org/software/wget/) Not needed if manually download reads from SRA.
    
    -   [curl](http://curl.haxx.se/download.html): Not needed if manually downloading genome and annotation from JGI

-   Python libraries

    All of these packages can be installed using [pip](https://pypi.python.org/pypi/pip) by running `pip install -r requirements.txt`, using the requirements.txt file included in the repository.
    
    -   biopython
    
    -   brewer2mpl
    
    -   HTSeq
    
    -   matplotlib
    
    -   numpy
    
    -   pandas
    
    -   pysam
    
    -   weblogo
    
    You might consider installing and running in a [Virtual Environment](https://pypi.python.org/pypi/virtualenv) (see [virtualenv guide](http://docs.python-guide.org/en/latest/dev/virtualenvs/)), in which case the following commands will get you running :
    
    1.  `git clone https://bitbucket.org/granek/mucor_srna.git`
    
    2.  `virtualenv venv`
        
        -   **Note:** If the default version of python on your system is older than 2.7, you might need to specify the path to python2.7, for example: `virtualenv -p /usr/local/bin/python2.7 venv`
    
    3.  `source venv/bin/activate`
    
    4.  `cd mucor_srna`
    
    5.  `pip install -r requirements_1.txt` (need to install numpy before HTSeq)
    
    6.  `pip install -r requirements_2.txt`
    
    7.  `make`

## Mucor circinelloides genome

The Mucor genome and annotation are required.  There are two options for doing this:

1.  Manual download:
    
    1.  Make a directory named `genome`.
    
    2.  Manually download the [Mucor genome](http://genome.jgi-psf.org/Mucci2/download/Mucor_circinelloides_v2_scaffolds.fasta.gz) to the `genome` directory.
    
    3.  Manually download the [Mucor annotation](http://genome.jgi.doe.gov/Mucci2/download/Mucor_circinelloides_v2_filtered_genes.gff.gz) to the `genome` directory.

2.  Automatic download:
    
    1.  Define "JGI\_USER=username" and "JGI\_PASS=password" in the makefile, where "username" and "password" are your username and password for the JGI website.
    
    2.  Run `make genome` or `make all`.

## Raw RNASeq data

Running `make` will automatically download the [raw data](http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE56353) from this work is available from [GEO](http://www.ncbi.nlm.nih.gov/geo/).

# Troubleshooting

## Problems running pdflatex fkba\_exon\_spanning\_reads.tex

While running `pdflatex fkba_exon_spanning_reads.tex` to generate fkba\_exon\_spanning\_reads.pdf you may encounter problems with memory limits of pdflatex.  If you get the following error:

> TeX capacity exceeded, sorry [save size=5000].

You will need to adjust this value in your texmf.cnf file.  

If you have root access to the machine, you can just alter the system wide configuration:

1.  Find the system-wide texmf.cnf using the command `kpsewhich -a texmf.cnf`.

2.  Edit the value for "save\_size = N".  A value of 50000 for N seems to work.

If you do not have root access to the machine, you can make a local texmf.cnf as described [here](http://people.debian.org/~preining/TeX/TeX-on-Debian/ch2.html#s-sec-user-config-files):

1.  Add this line to your ~/.bashrc file:      "export TEXMFCNF=$HOME/.texmf-config/web2c:"

2.  Create the file ~/.texmf-config/web2c/texmf.cnf

3.  Add the line "save\_size = 50000" to the file.

4.  Reload your bash configuration: `source ~/.bashrc`
