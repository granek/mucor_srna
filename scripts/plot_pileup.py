from optparse import OptionParser
import sys
import os
import pysam
import re
import csv
import brewer2mpl
import itertools
import numpy as np

#--------------------------------------------------
try:
    import matplotlib
    matplotlib.use('Agg')
    # import matplotlib
    import matplotlib.pyplot as plt
    from matplotlib.backends.backend_pdf import PdfPages
    from matplotlib.ticker import FuncFormatter, MaxNLocator
    import matplotlib.lines as mlines
    import matplotlib.transforms as transforms
    from matplotlib.patches import Rectangle
    import matplotlib.gridspec as gridspec
    from mpl_toolkits.mplot3d import Axes3D
    from matplotlib.collections import PolyCollection,LineCollection
    from matplotlib.colors import colorConverter
    # import matplotlib.font_manager 
except ImportError:
    print >>sys.stderr, "matplotlib not installed: can't use --plot"
#--------------------------------------------------
try:
    import HTSeq
except ImportError:
    print >>sys.stderr, "HTSeq not installed: can't use --gff"
#--------------------------------------------------

region_re = re.compile("([^:]+)(:(\d+)-(\d+))?")

ARRAYS = True
    
"""
# on microbe
python2.7 ~/collabs/scripts/read_size_distribution.py scaffold_03:1803135-1803768 /nfs/gems_sata/heitman/mucor_srna/tophatout/*/accepted_hits.bam --plot ~/collabs/HeitmanLab/mucor_srna/for_silvias_lab_meeting/read_count_distrib.pdf

#--------------------------------------------------
# on stanleybook pro
# whole chromosome
python2.7 plot_pileup.py  scaffold_03 ~/Documents/BioinfCollabs/HeitmanLab/mucor_srna/tophatout/EM2_1_TGACCA_L002_R1_001/accepted_hits.bam --plot ~/Documents/BioinfCollabs/HeitmanLab/mucor_srna/for_silvias_lab_meeting/pileup_em2_1__scaffold_03.pdf
python2.7 plot_pileup.py  scaffold_03 ~/Documents/BioinfCollabs/HeitmanLab/mucor_srna/tophatout/*/accepted_hits.bam --plot ~/Documents/BioinfCollabs/HeitmanLab/mucor_srna/for_silvias_lab_meeting/pileup_scaffold_03_all.pdf

# FkbA region testset
python2.7 plot_pileup.py  scaffold_03:1803135-1803800 ~/Documents/BioinfCollabs/HeitmanLab/mucor_srna/tophatout/*/accepted_hits.bam --gff ~/Documents/BioinfCollabs/HeitmanLab/mucor_srna/mcircinelloides_genome/Mucor_circinelloides_v2_genes_of_interest.gff --plot ~/Documents/BioinfCollabs/HeitmanLab/mucor_srna/for_silvias_lab_meeting/readstack_fkba_all_gff.pdf
"""
def main():
    usage = 'usage: %prog [-h/--help] [BAMFILE or --table BAMFILE_TABLE]  [--gene GENE or --region REGION]'
    parser = OptionParser(usage)
    parser.add_option("-t", "--table",action="store",metavar="BAMFILE_TABLE",
                      help="Load BAM files from BAMFILE_TABLE instead of from commandline (csv format: sample,path).")
    parser.add_option("-o", "--outfile",action="store",metavar="OUTFILE",
                      help="Save plot to OUTFILE.")
    parser.add_option("--gff",action="store",metavar="GFF_FILE",
                      help="Use GFF_FILE to annotate genes on plot.")
    parser.add_option("-r","--region",action="store",metavar="REGION",
                      help="Plot REGION (format is 'CHROMNAME:START-END'")
    # parser.add_option("-r","--region",action="append",metavar="REGION",
    #                   help="Plot REGION (format is 'CHROMNAME:START-END'")
    parser.add_option("-g","--gene",action="store",metavar="GENE",
                      help="Plot the region containing GENE.  GENE may be a single gene name or a comma-separated list.  Must be used with --gff")
    parser.add_option("--raw",action="store_false",default=True,
                      help="Use raw read counts (default: normalize by total sample reads, ?FPM?)")
    parser.add_option("-l", "--label",action="store_true",default=False,
                      help="Label exons with names (only relevant when using --gff)")
    parser.add_option("-a", "--autotitle",action="store_true",default=False,
                      help="Use --gene or --region argument as plot title.")
    # parser.add_option("-n", "--minus",action="store_true",default=False,
    #                   help="Region of interest is on the minus strand, so distribution is reverted")
    parser.add_option("--linewidth",action="store",metavar="WIDTH",type=float,default=2.0,
                      help="Set width of plot lines")
    parser.add_option("--width",action="store",metavar="WIDTH",type=float,
                      help="Set plot width")
    parser.add_option("--legendsize",action="store",metavar="SIZE",type=float,default=9,
                      help="Set legend font size")
    parser.add_option("--noxlabel",action="store_true",default=False,
                      help="Don't label x-axis")
    parser.add_option("--nolegend",action="store_true",default=False,
                      help="Don't include legend")
    parser.add_option("--subplot",action="store_true",default=False,
                      help="Split each sample into an individual subplot instead of overlaying.")
    parser.add_option("--pdf",action="store_true",default=False,
                      help="Also output a copy in PDF format")
    parser.add_option("--ymax",action="store",metavar="MAX",type=float,
                      help="Use MAX as the y range of the plot")
    parser.add_option("--lwvar",action="store",metavar="STEP",type=float,default=0,
                      help="Vary the linewidth of plots from back to front, by STEP")
    parser.add_option("--lsvar",action="store_true",
                      help="Vary the linestyle of plots")
    parser.add_option("--threed",action="store_true",
                      help="Make three dimensional plot")
    parser.add_option("--test",action="store_true",
                      help="Output raw data")
    parser.add_option("--figsize",type=float,nargs=2,
                      help="Dimensions of the figure (width height)")
    # parser.add_option("--independenty",action="store_true",
    #                   help="Scale the y-axis of each subplot individually")
 
    (options, args) = parser.parse_args()

    
    print " ".join(sys.argv)
    samplecolor_dict = {}
    samplename_dict = {}
    samfile_list = []
    if options.table:
        reader = csv.reader(open(options.table))
        for curline in reader:
            samplename = curline[0]
            bampath = os.path.expandvars(curline[1])
            samfile_list.append(bampath)
            samplename_dict[bampath] = samplename
            if len(curline) == 3:
                color_tuple = tuple([float(x.strip()) for x in curline[2].split(",")])
                # print "max(color_tuple):", max(color_tuple)
                if max(color_tuple) > 1:
                    color_tuple = tuple([float(x/255.0) for x in color_tuple])
                samplecolor_dict[bampath] = color_tuple
                
    elif len(args)>=1:
        samfile_list = args
    else:
        parser.print_help()
        sys.exit(1)

    # if options.linewidth:
    #     # matplotlib.rc('lines',linewidth=options.linewidth)

    if options.lsvar:
        linestyles =  ["-","--",":","-.","_"]
    else:
        linestyles =  ["-"]
    lscycler = itertools.cycle(linestyles)

    if options.gene:
        gene_list = options.gene.split(",")
        outfile = "__".join(gene_list) + ".readstack.pdf"
        if not options.gff:
            print >>sys.stderr, "In order to use --gene, a GFF file must be specified using --gff."
            sys.exit()
        gff_file = HTSeq.GFF_Reader(options.gff)
        feature_dict = {}
        for feature in gff_file:
            feature_dict.setdefault(feature.name,[]).append(feature)
        start=sys.maxint
        end=0
        for gene in gene_list:
            for feature in feature_dict[gene]:
                chrom = feature.iv.chrom
                start = min(start,feature.iv.start)
                end = max(end,feature.iv.end)
        print "Plotting region {0}:{1}-{2}".format(chrom,start,end)
        buffer = int((end-start) *0.02)
        start = start-buffer
        end = end+buffer
    elif options.region:
        region = options.region
        region_match = region_re.match(region)
        if region_match:
            chrom = region_match.group(1)
            if region_match.group(3) and region_match.group(4):
                start_b1 = int(region_match.group(3))
                end_b1 = int(region_match.group(4))
                outfile = "_".join((chrom,str(start_b1),str(end_b1)))+".readstack.pdf"
            elif region_match.group(2) == None:
                start_b1 = 1
                temp_samfile = pysam.Samfile(samfile_list[0])
                seqlen_dict = dict([(seq["SN"], int(seq["LN"])) for seq in temp_samfile.header["SQ"]])
                end_b1 = seqlen_dict[chrom]
                outfile = chrom+".readstack.pdf"
            else:
                print >>sys.stderr, "Problem matching region string:", region
                sys.exit(1)
        else:
            print >>sys.stderr, "Problem matching region string:", region
            sys.exit(1)

        # print "REGION:", chrom, start_b1, end_b1
        # chrom, start_end_b1 = region.split(":")
        # start_b1, end_b1 = map(int,start_end_b1.split("-"))
        # start_b1 = args[2]
        # end_b1 = args[3]
        start = start_b1 - 1 # 0-based index
        end = end_b1 -1  # 0-based index
    else:
        print >>sys.stderr, "Must supply --gene OR --region"
        sys.exit(1)

    if options.outfile:
        outfile = options.outfile

    ymax = 0
    data_tuples = []

    if options.figsize:
        fig = plt.figure(figsize=options.figsize)
    else:
        fig = plt.figure()
    if options.gff:
        outer_grid = gridspec.GridSpec(2, 1,wspace=0.0, hspace=0.0,height_ratios=[15, 1])
    else:
        outer_grid = gridspec.GridSpec(1, 1,wspace=0.0, hspace=0.0)

    if options.threed:
        verts = []
        zs = []
        # cc = lambda arg: colorConverter.to_rgba(arg, alpha=0.6)
        colors = []
    else:
        if options.subplot:
            ingrid_numrows = len(samfile_list)
        else:
            ingrid_numrows = 1
        inner_grid = gridspec.GridSpecFromSubplotSpec(ingrid_numrows, 1,subplot_spec=outer_grid[0],wspace=0.0, hspace=0.05)
        # fig, ax_list = plt.subplots(len(samfile_list), sharex=True, sharey=True)
        ax_list = []
        for i in range(ingrid_numrows):
            if i == 0:
                ax = plt.Subplot(fig, inner_grid[i])
                first_ax = ax
            else:
                ax = plt.Subplot(fig, inner_grid[i],sharex=first_ax,sharey=first_ax)
                # if options.independenty:
                #     ax = plt.Subplot(fig, inner_grid[i],sharex=first_ax)
                # else:
                #     ax = plt.Subplot(fig, inner_grid[i],sharex=first_ax,sharey=first_ax)
                # ax = plt.Subplot(fig, inner_grid[i],sharey=ax_list[0])
            ax_list.append(ax)
            fig.add_subplot(ax)

    if options.gff:
        gff_ax = plt.Subplot(fig, outer_grid[1], sharex=first_ax)
        fig.add_subplot(gff_ax)
        # pass
        # fig = plt.figure()
        # gs = gridspec.GridSpec(2, 1,height_ratios=[10, 1])
        # ax = fig.add_subplot(gs[0])
        # gff_ax = fig.add_subplot(outer_grid[1], sharex=ax)

        # # fig.subplots_adjust(hspace=0)
        # # http://stackoverflow.com/questions/2176424/hiding-axis-text-in-matplotlib-plots
        gff_ax.get_xaxis().set_visible(False)
        gff_ax.get_yaxis().set_visible(False)
        for side in gff_ax.spines:
            gff_ax.spines[side].set_visible(False)


    if options.width:
        orig_width,orig_height = fig.get_size_inches()
        new_width = options.width
        new_height = orig_height * new_width/float(orig_width)
        print "dimensions:", orig_width, orig_height, new_width, new_height
        fig.set_size_inches(new_width,new_height)


    # sample_tuple = []

    # print >>sys.stderr,  len(samfile_list)
    # colormap = brewer2mpl.get_map('Set1', 'Qualitative', len(samfile_list))
    if samplecolor_dict:
        # got colors from --table
        # color_list = ('black')
        # cmap=plt.get_cmap(name)
        color_list = plt.cm.Set1(np.linspace(0, 1, len(samfile_list)))
    elif len(samfile_list) < 3:
        color_list = ('black','red')
    else:
        colormap = brewer2mpl.get_map('Paired', 'Qualitative', len(samfile_list))
        color_list = colormap.mpl_colors
        # It might be useful to be able to interpolate colors for larger sample sizes
        # Something like this R snippet: colorRampPalette(brewer.pal(9,"Blues"))(100)
        # Python tools/code that might be helpful:
        # <http://markkness.net/colorpy/ColorPy.html>
        # <http://www.elifulkerson.com/projects/rgb-color-gradation.php>
        # <http://python-colormath.readthedocs.org/en/latest/>
        # http://stackoverflow.com/questions/168838/color-scaling-function
        # http://stackoverflow.com/questions/12432383/python-convert-values-between-0-and-1-to-color-range-red-to-blue
        
    for i,samfile_name in enumerate(samfile_list):
        head, tail = os.path.split(samfile_name)
        root,ext = os.path.splitext(tail)
        if ".bam" == ext.lower():
            samfile = pysam.Samfile(samfile_name,"rb")
        elif ".sam" == ext.lower():
            samfile = pysam.Samfile(samfile_name,"r")
        else:
            print >>sys.stderr, "Unknown extension:", tail
            sys.exit(1)

        print >>sys.stderr, "samfile.filename:", samfile.filename
        # sys.settrace
        if ARRAYS == True:
            print "USING ARRAYS"
            plus_count_array, minus_count_array, samplename, mapped_read_count, base_index = compute_pileup__arrays(samfile,chrom,start,end,options.raw)
            ymax = max(ymax, np.amax(plus_count_array), np.amax(minus_count_array))

            combined_array = plus_count_array + minus_count_array
            print samplename
            print "Max:", np.amax(combined_array)
            print "Median:", np.median(combined_array)
            print "Mean:", np.mean(combined_array)
            print "3rd quartile:", np.percentile(combined_array,q=75) #,interpolation='nearest')
            print "1st quartile:", np.percentile(combined_array,q=25) #,interpolation='nearest')


        else:
            print "USING DICTS"
            plus_count_dict, minus_count_dict, samplename, mapped_read_count = compute_pileup__dicts(samfile,chrom,start,end,options.raw)
            ymax = max(ymax, max(plus_count_dict.values()+minus_count_dict.values()))

        # 
        if options.test:
            test(plus_count_dict, minus_count_dict, plus_count_array, minus_count_array, base_index, samplename)

        samfile.close()
        samplecolor = samplecolor_dict.get(samfile_name,color_list[i])
        if options.table:
            samplename = samplename_dict[samfile_name]
            # samplecolor = samplecolor_dict.get(samfile_name,colormap.mpl_colors[i])
            # print "samplecolor:", type(samplecolor),samplecolor,type(colormap.mpl_colors[i]), colormap.mpl_colors[i]
        # data_tuples.append((samplename, plus_count_dict, minus_count_dict))
    
                           
        # if options.minus:
        #     sense_count_dict = minus_count_dict
        #     antisense_count_dict = plus_count_dict
        # else:
        #     sense_count_dict = plus_count_dict
        #     antisense_count_dict = minus_count_dict

        # Need to pad samples that don't have reads
        if options.subplot:
            ax = ax_list[i]
        linewidth = options.linewidth+(options.lwvar*i)
        if options.threed:
            zs.extend(map(float,(i,i)))
            sense_count_tuples, antisense_count_tuples = plot_3d(plus_count_dict,minus_count_dict)
            verts.append(sense_count_tuples)
            verts.append(antisense_count_tuples)
            colors.extend([samplecolor,samplecolor])
        else:
            if ARRAYS == True:
                curline = plot_distrib__arrays(plus_count_array,minus_count_array,base_index,ax,samplename,color=samplecolor,linewidth=linewidth,linestyle=next(lscycler))
            else:
                curline = plot_distrib__dicts(plus_count_dict,minus_count_dict,ax,samplename,color=samplecolor,linewidth=linewidth,linestyle=next(lscycler))

        # sample_tuple.append((curline,samplename))


    if options.threed:
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        
        # colors = [cc('r'), cc('g'), cc('b'),cc('y'),cc('r'), cc('g'), cc('b'),cc('y')]
        # for vert in verts:
        #     print vert, "\n"
        # print zs
        # print start,end
        poly = PolyCollection(verts, facecolors = colors)
        poly.set_alpha(0.8)
        ax.add_collection3d(poly, zs=zs, zdir='y')
        ax.set_xlim3d(start-1,end+1)
        ax.xaxis.set_major_formatter(FuncFormatter(lambda x, pos: '%.0f'%x))
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        # ax.set_ylim3d(-ymax, ymax)
        # ax.set_zlim3d(min(zs), max(zs))
        ax.set_zlim3d(-ymax, ymax)
        ax.set_ylim3d(min(zs), max(zs))
        ax.set_zlabel('Z')
        plt.show()
    if options.gff:
        # annotate_plot(options.gff,chrom,start,end,ax,options.label,options.noxlabel)
        annotate_plot(options.gff,chrom,start,end,gff_ax,options.label,options.noxlabel)
        # pass
                
    # print sample_tuple
    if options.autotitle:
        if options.region:
            title = options.region
        elif options.gene:
            title = options.gene
    else:
        title = None

    print "-"*40, "\nobserved YMAX:", ymax
    if options.ymax:
        ymax = options.ymax
        print "YMAX used:", ymax

    
    if options.subplot:
        finish_subplot(outfile,fig,ax_list,ymax,start,end,options.pdf,options.noxlabel)
        # ystep = ymax*2/ingrid_numrows
        # print "ystep:", ystep
        # # ax.yaxis.set_ticks(range(-int(ymax),int(ymax),int(ystep)))
        # for ax in ax_list:
        #     # ax.yaxis.set_ticks(range(-400,400,200))
        #     ax.yaxis.set_ticks([])
    else:
        finish_plot(outfile,fig,ax,ymax,start,end,options.legendsize,chrom,options.noxlabel,options.nolegend,options.pdf,options.raw,title)


def annotate_plot(gff_filename,chrom,start,end,ax,label,bottom=True):
    gff_file = HTSeq.GFF_Reader(gff_filename)
    # feature_dict = {}
    #--------------------------------------------------
    exons = HTSeq.GenomicArrayOfSets( "auto", stranded=False )
    # We populate the array again with the feature data. This time, we use the +=, which, for a GenomicArrayOfSets, does not mean numerical addition, but adds an object without overwriting what might already be there. Instead, it uses sets to deal with overlaps. (Also, we only store the gene name this time, as this will be more convenient later).
    for feature in gff_file:
        if feature.type in ("exon","3UTR"):
            exons[ feature.iv ] += feature
    # Find exons that intersect with interval defined by ROI
    iv = HTSeq.GenomicInterval(chrom,start,end+1, "." )  # The "end" of the interval is one more than the coordinate of the last base that is considered part of the sequence. (following Python convention for ranges)

    exons_in_region = set()
    for cur_iv, cur_set in exons[iv].steps():
        # print cur_iv
        # print cur_set
        exons_in_region.update(cur_set)
    # print "exons_in_region:", exons_in_region
    last_exon = None
    exons_in_region = list(exons_in_region)
    exons_in_region.sort(lambda x, y: cmp(x.name, y.name) or cmp(x.iv.start, y.iv.start))
    trans = transforms.blended_transform_factory(ax.transData, ax.transAxes)
    scale_factor = 0.4
    exon_dy = scale_factor
    utr_dy = scale_factor/2.0
    

    if bottom:
        # minus_strand_y = -scale_factor
        # plus_strand_y = minus_strand_y + scale_factor * 1.3
        #----------------------------------------
        # plus_strand_y = -exon_dy
        # minus_strand_y = plus_strand_y - (scale_factor * 1.3)
        # ax.set_ylim(0,0.2)
        scaled_headlen = (end-start)*scale_factor*0.04
        minus_strand_y = exon_dy/2.0
        plus_strand_y = minus_strand_y + scale_factor * 1.3
    else:
        scaled_headlen = (end-start)*scale_factor*0.4
        minus_strand_y = 1 + scale_factor/2.0
        plus_strand_y = minus_strand_y + scale_factor * 1.3
        
    
    print "HEAD LENGTH", scaled_headlen, end-start
    for cur_exon in exons_in_region:
        # print cur_exon.iv.start, cur_exon.iv.end, cur_exon.iv.strand
        if cur_exon.iv.strand == "+":
            x1 = cur_exon.iv.start
            x2 = cur_exon.iv.end
            # y = 0.06
            y = plus_strand_y
        elif cur_exon.iv.strand == "-":
            x2 = cur_exon.iv.start
            x1 = cur_exon.iv.end
            y = minus_strand_y
            # y = 0.026
        dx = x2-x1
        if abs(dx) < scaled_headlen:
            headlen = 0
        else:
            headlen = scaled_headlen
        # exon_line = mlines.Line2D((x1,x2), (y,y))
        # exon_draw = ax.arrow(x1,y,x2-x1,y-y, fc="k", ec="k",head_width=0.5, head_length=0.5 )
        # ax.arrow(x1,y,x2-x1,0, shape='full', width=100, length_includes_head=True, head_width=100.0,head_length=10.0 )
        # the x coords of this transformation are data, and the y coord are axes
        # headlen = 40
        if cur_exon.type == "exon":
            exon_arrow = ax.arrow(x1,y,dx,0, shape='full', width=exon_dy, length_includes_head=True, head_width=scale_factor,head_length=headlen, transform=trans,fc="black",alpha=0.9)
            # exon_arrow.set_clip_on(False)
        elif cur_exon.type == "3UTR":
            utr_y = y-(utr_dy/2.0) # +(exon_dy/2.0) - (utr_dy/2.0)
            box = Rectangle((x1,utr_y), dx, utr_dy,ec=None,lw=0,fc="grey",transform=trans,alpha=0.9)
            ## trans = transforms.blended_transform_factory(ax.transAxes,ax.transData)
            ax.add_patch(box)
            # box.set_clip_on(False)
            # ax.arrow(x1,y,dx,0, shape='full', width=scale_factor, length_includes_head=True, head_width=scale_factor,head_length=headlen, transform=trans,fc="blue",alpha=0.9)
            
        if label:
            ax.text(x1+(dx/2),y, cur_exon.name, transform=trans, fontsize=5,horizontalalignment='center',verticalalignment='center',color="white")

        # ax.arrow(x1,0.9,x2-x1,0, shape='full', transform=trans)
        # Arrow(2, 2, 1, 1, edgecolor='white')
        # ax.add_line(exon_draw)
        # print "cur_exon", cur_exon.name, cur_exon.iv
        if last_exon and last_exon.name == cur_exon.name:
            x3 = last_exon.iv.end
            x4 = cur_exon.iv.start
            # if cur_exon.iv.strand == "+":
            #     x3 = last_exon.iv.end
            #     x4 = cur_exon.iv.start
            # elif cur_exon.iv.strand == "-":
            #     x3 = last_exon.iv.end
            #     x4 = cur_exon.iv.start
            # print "last_exon:", last_exon.iv
            # print "exon_connect:", x3,"->",x4
            # print "cur_exon", cur_exon.name, cur_exon.iv
            # print "-"*40
            exon_connect = mlines.Line2D((x3,x4), (y,y),transform=trans,color="black", alpha=0.5)
            ax.add_line(exon_connect)
        last_exon = cur_exon

def compute_pileup__dicts(samfile,chrom,start,end,normalize=True):
    # for alignedread in samfile.fetch('chr1', 100, 120):
    plus_count_dict = dict(zip(range(start+1,end+2), [0]*((end-start)+1)))
    minus_count_dict = dict(zip(range(start+1,end+2), [0]*((end-start)+1)))
    mapped_read_count = samfile.mapped
    samfile_cl = samfile.header["PG"][0]["CL"]
    source_fasta = samfile_cl.split()[-1]
    samplename = "_".join(os.path.basename(source_fasta).split("_")[:2])
    print "SAMPLE:", samplename
    print "MAPPED:", samfile.mapped
    print "UNMAPPED:", samfile.unmapped
    region = "{0}:{1}-{2}".format(chrom,start,end)
    print "region:", region
    # for pile in samfile.pileup(region=region,truncate=True):
    for pile in samfile.pileup(chrom, start, end):
        # print 'coverage at base %s = %s' % (pile.pos , pile.n)
        pos_b1 = pile.pos + 1
        if (pile.pos < start) or (pile.pos > end):
            continue
        # print pos_b1, pile.n
        for pileread in pile.pileups:
            # print "pileread.qpos:", pileread.qpos
            aread = pileread.alignment
            # print aread.alen, aread.rlen, aread.is_reverse, aread.is_unmapped, aread.flag, aread.cigarstring, aread.cigar, aread.seq, aread.fancy_str(), aread.overlap(pile.pos,pile.pos+1)
            # print pile.pos,":",aread.pos, aread.aend, aread.alen, aread.rlen, aread.is_reverse, aread.is_unmapped, aread.flag, aread.cigarstring, aread.cigar, aread.seq, aread.overlap(pile.pos,pile.pos+1)
            #--------------------------------------------------
            # The following ignores reads that don't "overlap" the current base (i.e. they span it with a CIGAR skipped operation) and of course skips reads that are unmapped
            if aread.overlap(pile.pos,pile.pos+1)==1 and (not aread.is_unmapped): 
                if aread.is_reverse:
                        count_dict = minus_count_dict
                else:
                        count_dict = plus_count_dict
                count_dict[pos_b1] += 1

    if normalize:
        normval = mapped_read_count/float(1e6)
        plus_count_dict = dict([(pos,count/normval) for pos, count in plus_count_dict.items()])
        minus_count_dict = dict([(pos,count/normval) for pos, count in minus_count_dict.items()])

    # for strand, count_dict in (("POS", plus_count_dict),
    #                            ("NEG", minus_count_dict)):
    #     print strand, 'STRAND COUNTS'
    #     print ','.join(map(str,('sRNA_len','count')))
    #     pos_list = sorted(count_dict.keys())
    #     for pos in pos_list:
    #         print ','.join(map(str,(pos, count_dict[pos])))
    return plus_count_dict, minus_count_dict, samplename, mapped_read_count


#------------------------------------------------------------
def compute_pileup__arrays(samfile,chrom,start,end,normalize=True):
    base_index = np.arange(start+1,end+2,dtype=np.int)
    plus_count = np.zeros(len(base_index), dtype=np.int)
    minus_count = np.zeros(len(base_index), dtype=np.int)

    mapped_read_count = samfile.mapped
    samfile_cl = samfile.header["PG"][0]["CL"]
    source_fasta = samfile_cl.split()[-1]
    samplename = "_".join(os.path.basename(source_fasta).split("_")[:2])
    print "SAMPLE:", samplename
    print "MAPPED:", samfile.mapped
    print "UNMAPPED:", samfile.unmapped
    region = "{0}:{1}-{2}".format(chrom,start,end)
    print "region:", region
    for pile in samfile.pileup(chrom, start, end):
        pos_b1 = pile.pos + 1
        if (pile.pos < start) or (pile.pos > end):
            continue
        for pileread in pile.pileups:
            aread = pileread.alignment
            # The following ignores reads that don't "overlap" the current base (i.e. they span it with a CIGAR skipped operation) and of course skips reads that are unmapped
            if aread.overlap(pile.pos,pile.pos+1)==1 and (not aread.is_unmapped): 
                if aread.is_reverse:
                    minus_count[pos_b1-(start+1)] += 1
                else:
                    plus_count[pos_b1-(start+1)] += 1

    if normalize:
        normval = mapped_read_count/float(1e6)
        plus_count = plus_count/normval
        minus_count = minus_count/normval
    return plus_count, minus_count, samplename, mapped_read_count, base_index



def finish_plot(plotfile,fig,ax,ymax,xmin,xmax,legendsize, chrom,noxlabel,nolegend,pdf,normalized=False,title=None):
    if normalized:
        # plt.ylabel('Number of Reads (Normalized - FPM)')
        ax.set_ylabel('Reads per Million')
    else:
        ax.set_ylabel('Number of Reads')
    
    if title:
        # plt.title(title.replace(",","\n"),fontsize=12)
        # plt.figure(0) 
        # plt.title(title.replace(",","\n"),fontsize=12)
        plt.suptitle(title.replace(",","\n"),fontsize=12)
        # plt.tight_layout()
    ax.set_ylim(-ymax*1.05,ymax*1.05)
    # xbuffer = (xmax-xmin) * 0.02
    # ax.set_xlim(xmin-xbuffer,xmax+xbuffer)
    ax.set_xlim(xmin,xmax)
    #------------------------------------------------------------
    # Generate Legend
    handles, labels = ax.get_legend_handles_labels()
    # # reverse the order
    # leg = ax.legend(handles[::-1], labels[::-1],prop={'size':9},loc="best",fancybox=True)
    if nolegend == False:
        leg = ax.legend(handles, labels,prop={'size':legendsize},loc="best",fancybox=True)
        leg.get_frame().set_alpha(0.5)

    #------------------------------------------------------------
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_yaxis().tick_left()   # remove unneeded ticks
    ax.get_xaxis().tick_bottom()   # remove unneeded ticks
    # ax.get_yaxis().tick_left()
    ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: int(abs(x))))
    
    if noxlabel:
        ax.spines["bottom"].set_visible(False)
        ax.axes.get_xaxis().set_visible(False)
    else:
        plt.xlabel('Genome Position on ' + chrom )
        
        # Rotate x-axis labels
        ax.xaxis.set_major_formatter(FuncFormatter(lambda x, pos: '%.0f'%x))
        # locs, labels = plt.xticks()
        # plt.setp(labels, rotation=45)
        # plt.gcf().subplots_adjust(bottom=0.15)
    #------------------------------------------------------------
    # Add y=0 line
    # plt.axhline(color="black",linewidth=0.5)
    #------------------------------------------------------------
    # DONE
    # plt.tight_layout()


    # gff_ax = fig.axes[1]

    plt.savefig(plotfile,dpi=300,bbox_inches="tight")
    # pp = PdfPages(plotfile)
    # pp.savefig(fig)
    # pp.close()
    if pdf:
        pdfplotfile = os.path.splitext(plotfile)[0] + ".pdf"
        plt.savefig(pdfplotfile,dpi=300,bbox_inches="tight")


def plot_distrib__dicts(sense_count_dict,antisense_count_dict,ax,samplename,color,linewidth=None,linestyle="_"):
    if linewidth == None:
        linewidth = matplotlib.rcParams['lines.linewidth']
    # --------------------------------------------------
    # Plot Sense strand
    sense_count_tuples = sorted(sense_count_dict.items())
    # print sense_count_tuples
    xs,ys = zip(*sense_count_tuples)
    # ymax = max(ys)
    # print "ymax", ymax
    # print xs
    # print ys
    # if linewidth != None:
    #     base_line, = ax.plot(xs, ys,"-",label=samplename,linewidth=linewidth)
    # else:
    base_line, = ax.plot(xs, ys,linestyle,label=samplename,color=color,linewidth=linewidth,alpha=0.9)
    # base_line, = ax.plot(x, y, **kwargs)
    # ax.fill_between(x, 0.9*y, 1.1*y, facecolor=base_line.get_color(), alpha=0.5)
    # --------------------------------------------------
    # Plot Antisense strand
    antisense_count_tuples = sorted(antisense_count_dict.items())
    xs,ys = zip(*antisense_count_tuples)
    # ymax = max(ymax, max(ys))
    # print "ymax", ymax
    ys = [-y for y in ys]
    # print xs
    # print ys
    # ax.plot(xs, ys, "-", color=base_line.get_color(),linewidth=linewidth)
    ax.plot(xs, ys, linestyle, color=color,linewidth=linewidth,alpha=0.9)
    # ax.plot(xs, ys)
    # --------------------------------------------------
    return base_line

def plot_distrib__arrays(sense_count,antisense_count,base_index,ax,samplename,color,linewidth=None,linestyle="_"):
    if linewidth == None:
        linewidth = matplotlib.rcParams['lines.linewidth']
    # --------------------------------------------------
    # Plot Sense strand
    base_line, = ax.plot(base_index, sense_count,linestyle,label=samplename,color=color,linewidth=linewidth,alpha=0.9)
    # --------------------------------------------------
    # Plot Antisense strand
    ax.plot(base_index, -antisense_count, linestyle, color=color,linewidth=linewidth,alpha=0.9)
    # --------------------------------------------------
    return base_line


def finish_subplot(plotfile,fig,ax_list,ymax,start,end,pdf,noxlabel):
    # fig.subplots_adjust(hspace=0)
    # Next includes labels on the bottom subplot, following does not
    # plt.setp([ax.get_xticklabels() for ax in fig.axes[:-1]], visible=False)
    
    # fig.ylabel('Reads per Million')
    # plt.locator_params(axis = 'y', nbins = 2)
    fig.text(0.06, 0.5, 'Reads per Million', ha='center', va='center', rotation='vertical')
    for ax in ax_list:
        # x-axis
        ax.set_xlim(start,end)
        ax.spines["top"].set_visible(False)
        ax.spines["bottom"].set_visible(False)
        ax.get_xaxis().tick_bottom()   # remove unneeded ticks
        ax.xaxis.set_ticks_position('none')
        plt.setp(ax.get_xticklabels(), visible=False)
        ax.xaxis.set_major_formatter(FuncFormatter(lambda x, pos: '%.0f'%x))
        ax.set_ylim(-ymax*1.05,ymax*1.05)
        ax.yaxis.set_major_locator(MaxNLocator(4)) # limit the number of y-tick labels so they don't overlap
        ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: int(abs(x))))
        ax.spines["right"].set_visible(False)
        ax.get_yaxis().tick_left()   # remove unneeded ticks
        # ax.yaxis.set_ticks([])
        # reduce y-axis labels to limit overlap
        # ax.yaxis.set_ticks([-ymax,0,ymax])

        # ystep = ymax*2/ingrid_numrows
        # print "ystep:", ystep
        # # ax.yaxis.set_ticks(range(-int(ymax),int(ymax),int(ystep)))
        # for ax in ax_list:
        #     # ax.yaxis.set_ticks(range(-400,400,200))
        #     

    # if noxlabel:
    #     axes = fig.axes
    #     fig.axes[-1].axes.get_xaxis().set_visible(False)
    # else:
    #     axes = fig.axes[:-1]
    #     # plt.xlabel('Genome Position on ' + chrom )
    #     bottom_ax = fig.axes[-1]
    #     plt.xlabel('Genome Position')        
    #     bottom_ax.xaxis.set_major_formatter(FuncFormatter(lambda x, pos: '%.0f'%x))
    #     bottom_ax.spines["top"].set_visible(False)
    #     bottom_ax.spines["right"].set_visible(False)
    #     bottom_ax.spines["bottom"].set_visible(False)
    #     bottom_ax.get_yaxis().tick_left()   # remove unneeded ticks
    #     bottom_ax.get_xaxis().tick_bottom()   # remove unneeded ticks
    #     # Rotate x-axis labels
    #     # locs, labels = plt.xticks()
    #     # plt.setp(labels, rotation=45)
    #     # plt.gcf().subplots_adjust(bottom=0.15)
    # for ax in axes:
    #     ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: int(abs(x))))
    #     ax.spines["top"].set_visible(False)
    #     ax.spines["bottom"].set_visible(False)
    #     ax.spines["right"].set_visible(False)
    #     ax.get_yaxis().tick_left()   # remove unneeded ticks
    #     ax.get_xaxis().tick_bottom()   # remove unneeded ticks
    #     ax.xaxis.set_ticks_position('none')
    # if noxlabel:
    #     ax = fig.axes[-1]
    #     ax.spines["bottom"].set_visible(True)
    #     ax.axes.get_xaxis().set_visible(True)

    plt.savefig(plotfile,dpi=300,bbox_inches="tight")
    if pdf:
        pdfplotfile = os.path.splitext(plotfile)[0] + ".pdf"
        plt.savefig(pdfplotfile,dpi=300,bbox_inches="tight")


def plot_3d(sense_count_dict,antisense_count_dict):
    # Plot Sense strand
    sense_count_tuples = sorted(sense_count_dict.items())
    # Pad ys with 0's at each end, and add to xs appropriately
    xs,ys = zip(*sense_count_tuples)
    minx = min(xs)
    maxx = max(xs)
    xs =list(xs)
    xs.insert(0,minx-1)
    xs.append(maxx+1)

    ys = list(ys)
    ys.insert(0,0)
    ys.append(0)
    sense_count_tuples = list(zip(xs,ys))


    
    antisense_count_tuples = sorted(antisense_count_dict.items())

    # Pad ys with 0's at each end, and add to xs appropriately
    xs,ys = zip(*antisense_count_tuples)
    minx = min(xs)
    maxx = max(xs)
    xs =list(xs)
    xs.insert(0,minx-1)
    xs.append(maxx+1)

    ys = [-y for y in ys]
    ys.insert(0,0)
    ys.append(0)
    antisense_count_tuples = list(zip(xs,ys))
    # print sense_count_tuples
    # print antisense_count_tuples
    
    return sense_count_tuples, antisense_count_tuples


def test(plus_count_dict, minus_count_dict, plus_count_array, minus_count_array, base_index, samplename):
    result_writer = csv.writer(sys.stdout)

    print "#"*50, "\n##", samplename
    print "# PLUS", "="*30 
    plus_count_tuples = sorted(plus_count_dict.items())
    # print zip(*plus_count_tuples)
    xs,ys = zip(*plus_count_tuples)
    failure_count = 0
    for i in range(len(base_index)):
        if (xs[i] <> base_index[i]) or (ys[i] <>plus_count_array[i]):
            print "FAILED:",xs[i],ys[i],plus_count_array[i], base_index[i]
            failure_count += 1
    print "#", "MINUS", "="*30 
    minus_count_tuples = sorted(minus_count_dict.items())
    xs,ys = zip(*minus_count_tuples)
    for i in range(len(base_index)):
        if (xs[i] <> base_index[i]) or (ys[i] <>minus_count_array[i]):
            print "FAILED:"
            result_writer.writerow((xs[i],ys[i],minus_count_array[i], base_index[i]))
            failure_count += 1
    if failure_count ==0:
        print "PASSED:", samplename
    else:
        print samplename, "NUMBER OF FAILURES:", failure_count


if __name__ == "__main__":
    main()
