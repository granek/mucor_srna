import HTSeq
import argparse
import sys
import re
import os
# import csv
# import collections
import itertools
# ambiguous_re = re.compile("ambiguous\[(.*)\]")
# sample_re = re.compile("(.*)_[ACGT]{6}_L002_R1_001")

"""
python2.7 $SCRIPTS/find_3utrs.py $MUCOR/mucor_manuscript/mucor_srna/test/patA_fkbA_3RACE_seq.sam --gff $MUCOR/mucor_manuscript/mucor_srna/tmp/Mucor_circinelloides_v2_filtered_genes.gff 

"""


def main():
    parser = argparse.ArgumentParser(description="????")
    parser.add_argument("SAM_FILE", type=file, help="")
    parser.add_argument("-g", "--gff", type=file, help="Revise features generated from matches to not overlap existing features in GFF file")
    args = parser.parse_args()

    match_list = parse_samfile(args.SAM_FILE.name)
    print >>sys.stderr, "="*60
    for match in match_list:
        print >>sys.stderr, "MATCH:", match.get_gff_line()

    if args.gff:
        utr_list = modify_gff(args.gff,match_list)
        # print "="*60
        for utr in utr_list:
            sys.stdout.write(utr.get_gff_line())
    

def parse_samfile(sam_filename):
    sambase,samext = os.path.splitext(sam_filename)
    if samext == ".sam":
        align_seq = iter(HTSeq.SAM_Reader( sam_filename ))
    elif samext == ".bam":
        align_seq = iter(HTSeq.BAM_Reader( sam_filename ))
    else:
        print >>sys.stderr, "Problem with SAM/BAM File:", sam_filename
        sys.exit(1)

    match_list = []
    for align in align_seq:
        # print align, align.iv
        for cigar in align.cigar:
            if cigar.type == "M":
                cur_feature = HTSeq.GenomicFeature(align.read.name, "3RACE",cigar.ref_iv)
                match_list.append(cur_feature)
                # print cur_feature.get_gff_line()
                # print cigar.get_gff_line()
            # else:
            #     print >>sys.stderr, cigar.type, cigar.ref_iv, cigar
    return match_list

def modify_gff(gff_filename, match_list):
    # feature_array = HTSeq.GenomicArray( "auto", stranded=False, typecode='O' )
    feature_array = HTSeq.GenomicArrayOfSets( "auto", stranded=True)
    gff_file = HTSeq.GFF_Reader(gff_filename)
    for feature in gff_file:
        feature_array[feature.iv] += feature

    # print >>"\n\n\n", "+"*60
    utr_list = []
    for match in match_list:
        # print match
        # print "-"*20
        first_elem = last_elem = None
        for iv2, step_set in feature_array[match.iv].steps():
            last_elem = iv2, step_set
            if first_elem == None:
                first_elem = iv2, step_set
            # print iv2
            # for feature in step_set:
            #     print feature

        if match.iv.strand == "+":
            # print "LAST:", last_elem
            three_prime_elem = last_elem
        elif match.iv.strand == "-":
            # print "FIRST:", first_elem
            three_prime_elem = first_elem

        three_prime_iv,three_prime_set = three_prime_elem
        if len(three_prime_set) == 0:
            three_utr = HTSeq.GenomicFeature(match.name, "3UTR",three_prime_iv)
            three_utr.attr={'name':match.name}
            print >>sys.stderr, "FOUND 3UTR:", three_utr
            utr_list.append(three_utr)
        else:
            print >>sys.stderr, "3' set is not empty:", three_prime_set
            
    return utr_list

if __name__ == "__main__":
   main()

