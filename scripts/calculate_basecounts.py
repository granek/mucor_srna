from optparse import OptionParser
import sys
import os
import pysam

from generate_alignment_fasta import load_samfile_region, parse_region_string

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
# import matplotlib.lines as mlines
# import matplotlib.font_manager 
# except ImportError:
#     print >>sys.stderr, "matplotlib not installed: can't use --plot"


"""
python2.7 $SCRIPTS/calculate_basecounts.py scaffold_03:1803250-1803768 $MUCOR/tophatout/EM1_1_CGATGT_L002_R1_001/accepted_hits.bam --plot $MUCOR/mucor_manuscript/mucor_srna/tmp/basecount_plot.pdf
"""
def main():
    usage = 'usage: %prog [-h/--help] REGION BAMFILE'
    parser = OptionParser(usage)
    parser.add_option("-p", "--plot",action="store",metavar="PLOTFILE",
                      help="Save a bar chart to PLOTFILE of the distribution of the first base in reads.  If --minus is given, results are for minus strand reads, otherwise they are for plus strand reads.")
    parser.add_option("--basecounts",action="store",metavar="COUNTFILE",
                      help="Calculate the base counts in each position and output to COUNTFILE")
    parser.add_option("--scale",action="store",metavar="SCALE_FACTOR",default=1.0,type="float",
                      help="Scale plot dimensions by SCALE_FACTOR")
    parser.add_option("-r", "--raw",action="store_false",default=True,
                      help="Use raw read counts (default: normalize by total sample reads, ?FPM?)")
    parser.add_option("-m", "--minus",action="store_true",default=False,
                      help="Region of interest is on the minus strand, so distribution is reverted")
    (options, args) = parser.parse_args()

    # if options.plot:
    #     fig,ax = setup_plot(options.raw)
    #     sample_tuple = []

    if len(args)!=2:
        parser.print_help()
        sys.exit(1)

    region = args[0]
    samfile_name = args[1]
    chrom, start_b0, end_b0 = parse_region_string(region)

    ymax = 0
    head, tail = os.path.split(samfile_name)
    root,ext = os.path.splitext(tail)
    if ".bam" == ext.lower():
        samfile = pysam.Samfile(samfile_name,"rb")
    elif ".sam" == ext.lower():
        samfile = pysam.Samfile(samfile_name,"r")
    else:
        print >>sys.stderr, "Unknown extension:", tail
        sys.exit(1)
    pos_count_dict, neg_count_dict, samplename, mapped_read_count = compute_basecounts(samfile,chrom,start_b0,end_b0)

    
    ymax = max(ymax, max(pos_count_dict.values()+neg_count_dict.values()))
    if options.minus:
        sense_count_dict = neg_count_dict
        antisense_count_dict = pos_count_dict
        count_dict = neg_count_dict
    else:
        sense_count_dict = pos_count_dict
        antisense_count_dict = neg_count_dict
        count_dict = pos_count_dict

    print sense_count_dict
    print antisense_count_dict
    if options.plot:
        # generate_barplot(sense_count_dict[0],antisense_count_dict[0],options.plot)
        generate_barplot(count_dict[0],options.plot,options.scale)

def generate_barplot(count_dict,plotfile,scalefactor):
    bases = ("A","C","G","T")
    rna_bases = [x.replace("T","U") for x in bases]
    ind = np.arange(len(bases))

    fig = plt.figure()
    orig_size = fig.get_size_inches()
    fig.set_size_inches(orig_size*scalefactor)
    ax = fig.add_subplot(111)

    width = 0.8       # the width of the bars
    space = 0.1
    # width = 0.75       # the width of the bars
    # rects = ax.bar(ind, [count_dict[x] for x in bases], width, color='r')
    print >>sys.stderr, "count_dict.values()", count_dict.values()
    total_counts = sum(count_dict.values())
    percent_counts = [100*(count_dict[x]/float(total_counts)) for x in bases]
    # rects = ax.bar(ind, percent_counts, color='black')
    rects = ax.bar(ind+space, percent_counts, width,color='black')
    # rects = ax.bar(ind, [count_dict[x] for x in bases], color='black')
    

    # add some
    # ax.set_ylabel('Percent')
    ax.set_ylabel('%')
    # ax.set_xlabel('First Nucleotide in Read')
    # ax.set_title('Scores by group and gender')
    ax.set_xticks(ind+space+width/2)
    ax.set_xticklabels(rna_bases)


    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_yaxis().tick_left()   # remove unneeded ticks
    ax.get_xaxis().tick_bottom()   # remove unneeded ticks
    ax.yaxis.set_ticks(range(0,91,30))
    ax.xaxis.set_ticks_position('none')
    # ax.legend((sense_rects[0], antisense_rects[0]), ('Sense', 'Antisense') )

    # plt.tight_layout()
    plt.savefig(plotfile,dpi=300,bbox_inches="tight")

def compute_basecounts(samfile,chrom,start_b0,end_b0):
    pos_count_dict = {}
    neg_count_dict = {}
    mapped_read_count = samfile.mapped
    samfile_cl = samfile.header["PG"][0]["CL"]
    source_fasta = samfile_cl.split()[-1]
    samplename = "_".join(os.path.basename(source_fasta).split("_")[:2])
    print "SAMPLE:", samplename
    print "MAPPED:", samfile.mapped
    print "UNMAPPED:", samfile.unmapped
    for alignedread in load_samfile_region(samfile,chrom,start_b0,end_b0):
        # if alignedread.rlen != len(alignedread.seq):
        #     print alignedread.alen, alignedread.rlen, alignedread.is_reverse, alignedread.is_unmapped, alignedread.flag, alignedread.cigarstring, alignedread.seq
        if not alignedread.is_unmapped:
            # rlen = alignedread.rlen
            if alignedread.is_reverse:
                count_dict = neg_count_dict
            else:
                count_dict = pos_count_dict
            for i,base in enumerate(alignedread.seq):
                base = base.upper()
                position_dict = count_dict.setdefault(i,{"A": 0,"C": 0,"G": 0,"T": 0,"N": 0})
                position_dict[base] += 1
    return pos_count_dict, neg_count_dict, samplename, mapped_read_count


if __name__ == "__main__":
    main()
