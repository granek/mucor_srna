import argparse
# import csv
import os
# import string
import sys
import subprocess
import re
import gzip
import pysam
import pandas as pd
import signal

gzip_re = re.compile(".*\.gz")
fastq_re = re.compile("(.*).fastq(.gz)?")
accepted_re = re.compile("(.*)accepted_hits.bam")
unampped_re = re.compile("(.*)unmapped.bam")

FILTERED = "filtered_reads"
RAW = "raw_reads"
MAPPED = "mapped_reads"
UNMAPPED = "unmapped_reads"

FILETYPE_LIST = (RAW,FILTERED,MAPPED,UNMAPPED)

# WT_ATCACG,EM1_1_CGATGT,EM2_1_TGACCA,EM3_1_ACAGTG,EM3_2_GCCAAT,EM1_2_TTAGGC
# samples = "WT_ATCACG","EM1_1_CGATGT","EM1_2_TTAGGC","EM3_1_ACAGTG","EM2_1_TGACCA","EM3_2_GCCAAT"
# python2.7 /home/josh/collabs/scripts/summary_statistics.py test_makefile_9_2_13/bamfiles/*/*.bam test_makefile_9_2_13/temp/fastqs__short/*.gz --filtered 3clip --samples WT_ATCACG,EM1_1_CGATGT,EM2_1_TGACCA,EM3_1_ACAGTG,EM3_2_GCCAAT,EM1_2_TTAGGC --outfile test_makefile_9_2_13/summary_stats.csv
# python2.7 /home/josh/collabs/scripts/summary_statistics.py test_makefile_9_2_13/bamfiles/*/*.bam test_makefile_9_2_13/fastqs/*.gz --filtered 3clip --samples WT_ATCACG,EM1_1_CGATGT,EM2_1_TGACCA,EM3_1_ACAGTG,EM3_2_GCCAAT,EM1_2_TTAGGC --outfile test_makefile_9_2_13/summary_stats.csv


def main():
    parser = argparse.ArgumentParser(description="Warning unmapped.bam does not contain multi-mapped reads for TopHat >= v2.0.9")
    parser.add_argument("INFILE",help="File(s) to count reads in. Can be FASTQ, FASTQ.gz, or Tophat accepted_hits.bam or unmapped.bam",nargs="+")
    parser.add_argument("--filtered", metavar="STRING", help="If $(metavar)s is found in a FASTQ filename, it is identified as a filtered FASTQ")
    parser.add_argument("--samples", metavar="SAMPLE_LIST", help="$(metavar)s is a comma-separated list of sample names, which are used to group results")
    parser.add_argument("--outfile", metavar="FILENAME", help="Save CSV formatted results to $(metavar)s")

    args = parser.parse_args()

    if args.samples:
        sample_list = args.samples.split(",")
        # sample_dict = {x:y for x,y in ((1,2),(3,4))}
        # sample_dict = {sample:{filetype:None for filetype in FILETYPE_LIST} for sample in sample_list}
        sample_df = pd.DataFrame(index=sample_list, columns=FILETYPE_LIST)
    else:
        sample_list = []
    sample_re = re.compile("({0})".format("|".join(sample_list)))
    
    for filename in args.INFILE:
        filetype = None
        read_count = None
        sample_name = None
        if args.filtered and fastq_re.match(filename) and args.filtered in filename:
            total_lines = wccount(filename)
            read_count = total_lines/4
            filetype = FILTERED
        elif fastq_re.match(filename):
            total_lines = wccount(filename)
            read_count = total_lines/4
            filetype = RAW
        elif accepted_re.match(filename):
            accepted_samfile = pysam.Samfile( filename, "rb" )
            read_count = accepted_samfile.mapped
            accepted_samfile.close()
            filetype = MAPPED
        elif unampped_re.match(filename):
            unmapped_samfile = pysam.Samfile( filename, "rb")
            read_count = unmapped_samfile.unmapped
            unmapped_samfile.close()
            filetype = UNMAPPED
        else:
            print "PROBLEM:", filename
            sys.exit(1)
        sample_match = sample_re.search(filename)
        if sample_match:
            sample_name = sample_match.group(1)
            sample_df.loc[sample_name,filetype] = read_count
        print sample_name, filetype, read_count, " " *20, filename
    if args.samples:
        print sample_df
        if args.outfile:
            sample_df.to_csv(args.outfile)

def countlines(filename):
    numlines = 0
    if gzip_re.match(filename):
        handle = gzip.open(filename, 'rb')
    else:
        handle = open(filename)
    for line in handle:
        numlines += 1
    return numlines

def wccount(filename):
    # Based on https://gist.github.com/zed/0ac760859e614cd03652
    numlines = 0
    if gzip_re.match(filename):
        pzcat = subprocess.Popen(["gunzip","-c", filename],
                                 stdout=subprocess.PIPE,
                                 preexec_fn=lambda:signal.signal(signal.SIGPIPE, signal.SIG_DFL))
        pwc = subprocess.Popen(['wc', '-l'], stdin=pzcat.stdout,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT
                               ).communicate()[0]
        out = pwc
    else:
        out = subprocess.Popen(['wc', '-l', filename],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
        ).communicate()[0]
    return int(out.partition(b' ')[0])

if __name__ == "__main__":
    main()

