from __future__ import division

import argparse

from Bio import SeqUtils
from Bio import SeqIO

from Bio.Alphabet import IUPAC
from Bio.Seq import Seq
import re

from generate_alignment_fasta import parse_region_string
"""
Based on:
----------
Introns and splicing elements of five diverse fungi.

Doris M Kupfer, Scott D Drabenstot, Kent L Buchanan, Hongshing Lai, Hua Zhu, David W Dyer, Bruce A Roe, Juneann W Murphy.

Genomic sequences and expressed sequence tag data for a diverse group of fungi (Saccharomyces cerevisiae, Schizosaccharomyces pombe, Aspergillus nidulans, Neurospora crassa, and Cryptococcus neoformans) provided the opportunity to accurately characterize conserved intronic elements. An examination of large intron data sets revealed that fungal introns in general are short, that 98% or more of them belong to the canonical splice site (ss) class (5'GU...AG3'), and that they have polypyrimidine tracts predominantly in the region between the 5' ss and the branch point. Information content is high in the 5' ss, branch site, and 3' ss regions of the introns but low in the exon regions adjacent to the introns in the fungi examined. The two yeasts have broader intron length ranges and correspondingly higher intron information content than the other fungi. Generally, as intron length increases in the fungi, so does intron information content. Homologs of U2AF spliceosomal proteins were found in all species except for S. cerevisiae, suggesting a nonconventional role for U2AF in the absence of canonical polypyrimidine tracts in the majority of introns. Our observations imply that splicing in fungi may be different from that in vertebrates and may require additional proteins that interact with polypyrimidine tracts upstream of the branch point. Theoretical protein homologs for Nam8p and TIA-1, two proteins that require U-rich regions upstream of the branch point to function, were found. There appear to be sufficient differences between S. cerevisiae and S. pombe introns and the introns of two filamentous members of the Ascomycota and one member of the Basidiomycota to warrant the development of new model organisms for studying the splicing mechanisms of fungi. - Eukaryotic Cell (2004) vol. 3 (5) pp. 1088-100

http://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?dbfrom=pubmed&id=15470237&retmode=ref&cmd=prlinks
"""


def main():
    parser = argparse.ArgumentParser(description="Find putative intron branch sites")
    parser.add_argument("REGION", action="store",
                         metavar="CHROM:START-END",
                         help="Search within region (one-based)")
    parser.add_argument("FASTA_FILE", type=file, help="Sequence to search (Multi-FASTA format)")
    args = parser.parse_args()

    record_iter = SeqIO.parse(args.FASTA_FILE, "fasta")
    
    chrom, start_b0, end_b0 = parse_region_string(args.REGION)
    seq = extract_region(chrom, start_b0, end_b0, record_iter)
    print "Sequence to Search:", seq, "\n"
    find_branches_in_seq(seq,start_b0+1)

def extract_region(chrom, start, end, record_iter):
    for record in record_iter:
        if record.id == chrom:
            return record.seq[start:end]
    return None

def find_branches_in_seq(seq,offset=0):
    branch1 = Seq("CTRAY", IUPAC.ambiguous_dna)
    branch1_rc = branch1.reverse_complement()
    branch2 = Seq("TTRAY", IUPAC.ambiguous_dna)
    branch2_rc = branch2.reverse_complement()
    not_found = []
    sites_found = []
    
    for site,rc in ((branch1,False),
                    (branch2,False),
                    (branch1_rc,True),
                    (branch2_rc,True)):
        branch_result = SeqUtils.nt_search(str(seq), site)
        if len(branch_result) > 1:
            # show_result(site, branch_result, seq,revcomp=rc, offset=offset)
            for pos in branch_result[1:]:
                actual_start = pos+offset
                actual_end = actual_start+len(site)-1
                output = "Found {0} ({1}) at {2}-{3}".format(seq[pos:pos+len(site)],site,actual_start,actual_end)
                sites_found.append((actual_start,actual_end))
                if rc is True:
                    output += " (reverse complement of {0})".format(site.reverse_complement())
                print output
        else:
            not_found.append(site)
    print "\nNot found:", ', '.join(map(str,not_found))
    return sites_found
    
if __name__ == "__main__":
    main()
