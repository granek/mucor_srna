from optparse import OptionParser
import sys
import os
import pysam
import csv
from generate_alignment_fasta import load_samfile_region, parse_region_string
READLEN_COL="read_len"

try:
    import numpy as np
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    from matplotlib.backends.backend_pdf import PdfPages
    # import matplotlib.lines as mlines
    # import matplotlib.font_manager 
except ImportError:
    print >>sys.stderr, "matplotlib not installed: can't use --plot"


"""
python2.7 ~/collabs/scripts/read_size_distribution.py scaffold_03:1803135-1803768 /nfs/gems_sata/heitman/mucor_srna/tophatout/*/accepted_hits.bam --plot ~/collabs/HeitmanLab/mucor_srna/for_silvias_lab_meeting/read_count_distrib.pdf
"""
def main():
    usage = 'usage: %prog [-h/--help] REGION BAMFILE'
    parser = OptionParser(usage)
    parser.add_option("--barplot",action="store",metavar="PLOTFILE STRAND",nargs=2,
                      help="Generate a barplot and save to PLOTFILE. STRAND must be: sense, antisense, or both")
    parser.add_option("-p", "--plot",action="store",metavar="PLOTFILE",
                      help="Generate a plot and save to PLOTFILE.")
    parser.add_option("--csv",action="store",metavar="CSVFILE",
                      help="Generate a table of readcounts and save to CSVFILE.")
    parser.add_option("--raw",action="store_false",default=True,
                      help="Use raw read counts (default: normalize by total sample reads, FPM)")
    parser.add_option("-n", "--negative",action="store_true",default=False,
                      help="Region of interest is on the negative strand, so distribution is reverted")
    # parser.add_option("--region",action="store",metavar="GENOME_REGION",
    #                   help="Output results from REGION of genome")
    parser.add_option("--scale",action="store",metavar="SCALE_FACTOR",default=1.0,type="float",
                      help="Scale plot dimensions by SCALE_FACTOR")
    
    (options, args) = parser.parse_args()

    # if options.region:
    #     print >>sys.stderr, "NEED TO CONVERT TO BASE1?"
    #     print >>sys.stderr, "SEE: <http://www.cgat.org/~andreas/documentation/pysam/glossary.html#term-region>"
    #     sys.exit(1)
        
    if options.plot or options.barplot:
        fig,ax = setup_plot(options.scale,options.raw)
    if len(args)>=2:
        region = args[0]
        samfile_list = args[1:]
        # chrom, start_end_b1 = region.split(":")
        # start_b1, end_b1 = map(int,start_end_b1.split("-"))
        # # start_b1 = args[2]
        # # end_b1 = args[3]
        # start = start_b1 - 1 # 0-based index
        # end = end_b1 -1  # 0-based index
        chrom, start_b0, end_b0 = parse_region_string(region)

        ymax = 0
        count_table_dict = {}
        samplename_list = []
        for samfile_name in samfile_list:
            head, tail = os.path.split(samfile_name)
            root,ext = os.path.splitext(tail)
            if ".bam" == ext.lower():
                samfile = pysam.Samfile(samfile_name,"rb")
            elif ".sam" == ext.lower():
                samfile = pysam.Samfile(samfile_name,"r")
            else:
                print >>sys.stderr, "Unknown extension:", tail
                sys.exit(1)
            pos_count_dict, neg_count_dict, samplename, mapped_read_count = compute_read_distrib(samfile,chrom,start_b0,end_b0,options.raw)
            ymax = max(ymax, max(pos_count_dict.values()+neg_count_dict.values()))
            if options.negative:
                sense_count_dict = neg_count_dict
                antisense_count_dict = pos_count_dict
            else:
                sense_count_dict = pos_count_dict
                antisense_count_dict = neg_count_dict
            if options.plot:
                curline = plot_distrib(sense_count_dict,antisense_count_dict,ax,samplename)
            if options.barplot:
                curline = barplot_distrib(sense_count_dict,antisense_count_dict,ax,samplename,options.barplot[1])
            if options.csv:
                count_table_dict = output_table(sense_count_dict,antisense_count_dict,samplename,count_table_dict)
                samplename_list.append(samplename)
                    
    else:
        parser.print_help()
        sys.exit(1)

    if options.plot:
        finish_plot(options.plot,fig,ax,ymax)
    if options.barplot:
        finish_barplot(options.barplot[0],fig,ax,ymax)
    if options.csv:
        finish_outtable(options.csv,count_table_dict,samplename_list)
    
def compute_read_distrib(samfile,chrom,start_b0,end_b0,normalize=True):
    # for alignedread in samfile.fetch('chr1', 100, 120):
    pos_count_dict = {}
    neg_count_dict = {}
    mapped_read_count = samfile.mapped
    print >>sys.stderr, samfile.header
    header = samfile.header
    if ("PG" in header) and ("CL" in header["PG"][0]):
        samfile_cl = samfile.header["PG"][0]["CL"]
        source_fasta = samfile_cl.split()[-1]
        samplename = "_".join(os.path.basename(source_fasta).split("_")[:2])
    else:
        samplename = os.path.splitext(os.path.basename(samfile.filename))[0]
    print "SAMPLE:", samplename
    print "MAPPED:", samfile.mapped
    print "UNMAPPED:", samfile.unmapped
    # for alignedread in samfile.fetch(region=region):
    # for alignedread in samfile.fetch(chrom, start_b0, end_b0):
    for alignedread in load_samfile_region(samfile,chrom,start_b0,end_b0):

        # if alignedread.rlen != len(alignedread.seq):
        #     print alignedread.alen, alignedread.rlen, alignedread.is_reverse, alignedread.is_unmapped, alignedread.flag, alignedread.cigarstring, alignedread.seq
        if not alignedread.is_unmapped:
            rlen = alignedread.rlen
            if alignedread.is_reverse:
                count_dict = neg_count_dict
            else:
                count_dict = pos_count_dict
            count_dict[rlen] = count_dict.get(rlen,0) + 1
    samfile.close()

    if normalize:
        normval = mapped_read_count/float(1e6)
        pos_count_dict = dict([(rlen,count/normval) for rlen, count in pos_count_dict.items()])
        neg_count_dict = dict([(rlen,count/normval) for rlen, count in neg_count_dict.items()])

    for strand, count_dict in (("POS", pos_count_dict),
                               ("NEG", neg_count_dict)):
        print strand, 'STRAND COUNTS'
        print ','.join(map(str,('sRNA_len','count')))
        rlen_list = sorted(count_dict.keys())
        for rlen in rlen_list:
            print ','.join(map(str,(rlen, count_dict[rlen])))
    return pos_count_dict, neg_count_dict, samplename, mapped_read_count

def setup_plot(scalefactor,normalized=False):
    fig = plt.figure()
    orig_size = fig.get_size_inches()
    fig.set_size_inches(orig_size*scalefactor)
    plt.xlabel('sRNA Length')
    if normalized:
        plt.ylabel('Reads per Million')
    else:
        plt.ylabel('Total Reads')
    ax = fig.add_subplot(111)
    return fig,ax

def finish_plot(plotfile,fig,ax,ymax):
    #------------------------------------------------------------
    # Generate Legend
    handles, labels = ax.get_legend_handles_labels()
    # reverse the order
    ax.legend(handles[::-1], labels[::-1])
    ax.set_ylim(-ymax*1.05,ymax*1.05)
    ax.legend(handles[::-1], labels[::-1])
    #------------------------------------------------------------
    # Add Sense/Antisense Annotation
    ax.text(0.025, 0.975, 'Sense Reads',
        verticalalignment='top', horizontalalignment='left',
        transform=ax.transAxes,
        color='black', fontsize=15,
        bbox={'facecolor':'white', 'alpha':0.5, 'pad':10})

    ax.text(0.025, 0.025, 'Antisense Reads',
        verticalalignment='bottom', horizontalalignment='left',
        transform=ax.transAxes,
        color='white', fontsize=15,
        bbox={'facecolor':'black', 'alpha':0.5, 'pad':10})

    # ax.annotate('annotate', xy=(2, 1), xytext=(3, 4),
    #             arrowprops=dict(facecolor='black', shrink=0.05))
    
    #------------------------------------------------------------
    # DONE
    pp = PdfPages(plotfile)
    pp.savefig(fig)
    pp.close()

def plot_distrib(sense_count_dict,antisense_count_dict,ax,samplename):
    # --------------------------------------------------
    # Plot Sense strand
    sense_count_tuples = sorted(sense_count_dict.items())
    xs,ys = zip(*sense_count_tuples)
    # ymax = max(ys)
    # print "ymax", ymax
    print xs
    print ys
    base_line, = ax.plot(xs, ys,"o-",label=samplename)
    # base_line, = ax.plot(x, y, **kwargs)
    # ax.fill_between(x, 0.9*y, 1.1*y, facecolor=base_line.get_color(), alpha=0.5)
    # --------------------------------------------------
    # Plot Antisense strand
    antisense_count_tuples = sorted(antisense_count_dict.items())
    xs,ys = zip(*antisense_count_tuples)
    # ymax = max(ymax, max(ys))
    # print "ymax", ymax
    ys = [-y for y in ys]
    print xs
    print ys
    ax.plot(xs, ys, "o-", color=base_line.get_color())
    # ax.plot(xs, ys)
    # --------------------------------------------------
    return base_line

def barplot_distrib(sense_count_dict,antisense_count_dict,ax,samplename,strand):
    if strand.lower() == "sense":
        total_count_dict = sense_count_dict
    elif strand.lower() == "antisense":
        total_count_dict = antisense_count_dict
    elif strand.lower() == "both":
        total_count_dict = sense_count_dict.copy()
        for size in antisense_count_dict:
            total_count_dict[size] = total_count_dict.get(size,0) + antisense_count_dict[size]
    else:
        raise StandardError, "Unknown argument for STRAND:", strand
    # --------------------------------------------------
    # Plot Sense strand
    count_tuples = sorted(total_count_dict.items())
    xs,ys = zip(*count_tuples)
    # ymax = max(ys)
    # print "ymax", ymax
    print xs
    print ys

    width = 0.6       # the width of the bars
    space = 0.1

    # ind = np.arange(len(ys))
    rects = ax.bar(xs, ys, width,color='black')
    # plt.xticks(np.array(xs)+0.25,xs)
    locs,labels =  plt.xticks()
    print "plt.xticks:", locs, labels, dir(labels), str(labels)
    plt.xticks(locs+0.25,locs.astype(int))
    #------------------------------------------------------------ 
    # total_count_list = []
    # for key in total_count_dict:
    #     total_count_list.extend([key]*int(total_count_dict[key]))
    # n, bins, patches = plt.hist(total_count_list, len(total_count_dict), histtype='bar')
    #------------------------------------------------------------
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_yaxis().tick_left()   # remove unneeded ticks
    ax.get_xaxis().tick_bottom()   # remove unneeded ticks
    ax.xaxis.set_ticks_position('none')


def finish_barplot(plotfile,fig,ax,ymax):
    # fig = plt.figure()
    # orig_size = fig.get_size_inches()
    # fig.set_size_inches(orig_size/3)


    # plt.savefig(plotfile,dpi=300)
    plt.savefig(plotfile,dpi=300,bbox_inches="tight")
    
    # plt.tight_layout()
    # pp = PdfPages(plotfile)
    # pp.savefig(fig)
    # pp.close()

def output_table(sense_count_dict,antisense_count_dict,samplename,count_table_dict):
    count_table_dict = count_table_dict.copy()
    # Sense strand counts
    for readlen,readcount in sense_count_dict.items():
        count_table_dict.setdefault(readlen,{})[samplename] = readcount
        count_table_dict[readlen][READLEN_COL] = readlen
    # Antisense strand counts
    for readlen,readcount in antisense_count_dict.items():
        count_table_dict.setdefault(-readlen,{})[samplename] = readcount
        count_table_dict[-readlen][READLEN_COL] = -readlen
    return count_table_dict

def finish_outtable(outfile,count_table_dict,fieldnames):
    print count_table_dict
    outhandle = open(outfile,'w')
    outwriter = csv.DictWriter(outhandle, [READLEN_COL]+fieldnames, restval=0)
    outwriter.writeheader()
    for readlen in sorted(count_table_dict.keys()):
        outwriter.writerow(count_table_dict[readlen])
   
if __name__ == "__main__":
    main()
