# from optparse import OptionParser
import sys
import os
import cStringIO
import argparse
from generate_alignment_fasta import parse_region_string

# from weblogolib import *
import pysam
import weblogolib
 
# def main():
#     usage = 'usage: %prog [-h/--help] SEQFILE'
#     parser = OptionParser(usage)
#     (options, args) = parser.parse_args()

#     if len(args)==1:
#         seq_file = args[0]
#         seq_handle = open(seq_file,'Ur')
#     else:
#         parser.print_help()
#         sys.exit(1)


def main():
    parser = argparse.ArgumentParser(description="Generate sequence logos from input sequences")
    parser.add_argument("SEQFILE", type=file, help="SAM/BAM file (ends in .bam or .sam) or flat sequence file (one sequence per line)")

    parser.add_argument("--strand", help="Strand of reads to consider.",choices=['plus','minus','both'],default="both")
    # mutex_group.add_argument("FLATFILE", type=file,help="Flat sequence file (one sequence per line)")

    # parser.add_argument("OUTFILE", help="Name to save to.  If ends in '.gz', output will be gzipped")
    
    parser.add_argument("REGION",nargs="?",help="Region of SAM/BAM to generate logos from")
    parser.add_argument("--outdir",metavar="PATH",help="Save to to %(metavar)s.")
    parser.add_argument("--outroot",metavar="ROOT",help="Use %(metavar)s. as the root for naming output files")
    parser.add_argument("--verbose", help="increase output verbosity",action="store_true")
    # parser.add_argument("-o", "--outfile",metavar="FILENAME",
    #                      help="Convert FASTQ[s] to Phred+33 and save to %(metavar)s. " +
    #                      "Multiple input files will be concatenated. " +
    #                      "If %(metavar)s ends in '.gz', output will be gzipped")
    # parser.add_argument("-s", "--sample",metavar="NUM",default=None,type=int,
    #                      help="Only test the first %(metavar) quality scores in each FASTQ")
    args = parser.parse_args()

    # seqfile_name = args.SEQFILE.name
    head, tail = os.path.split(args.SEQFILE.name)
    root,ext = os.path.splitext(tail)
    if ".bam" == ext.lower() or ".sam" == ext.lower():
        samfile = pysam.Samfile(args.SEQFILE.name)
        seq_dict = load_sequences_from_sam(samfile,args.REGION,args.strand,args.verbose)
    # elif ".sam" == ext.lower():
    #     samfile = pysam.Samfile(args.SEQFILE.name,"r")
    #     seq_dict = load_sequences_from_sam(samfile,args.REGION,args.strand,args.verbose)
    else:
        seq_dict = load_sequences_from_flat_file(args.SEQFILE)
    # if args.FLATFILE:
    #     seq_dict = load_sequences_from_flat_file(args.FLATFILE)
    # elif args.samfile:
    #     samfile = pysam.Samfile(args.samfile.name)
    #     seq_dict = load_sequences_from_sam(samfile,args.REGION)
    # else:
    #     print "No input file?"
    #     sys.exit(1)
    outroot,outdir = determine_outnames(args.SEQFILE.name,args.outdir,args.outroot)
    iterate_read_sizes(seq_dict,outroot,args.strand,outdir)

def determine_outnames(infile_path,outdir,root):
    head, tail = os.path.split(infile_path)
    if not root:
        root,ext = os.path.splitext(tail)
    if not outdir:
        outdir = os.path.join(head,root)
    print "Saving to", outdir
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    return root,outdir

def iterate_read_sizes(seq_dict,outroot,strand,outdir):
    count_out = open(os.path.join(outdir,outroot+'_sizedist.csv'), 'w')
    logolist_out = open(os.path.join(outdir,outroot+'_logolist.txt'), 'w')
    
    print >>count_out, ','.join(map(str,('sRNA_len','count')))
    for seqsize in sorted(seq_dict.keys()):
        readlist = seq_dict[seqsize]
        print seqsize, readlist
        logo_handle = open(os.path.join(outdir,'_'.join((outroot,strand,str(seqsize)+'.pdf'))), 'w')
        print >>count_out, ','.join(map(str,(seqsize, len(readlist))))
        print >>logolist_out, logo_handle.name
        generate_logo_from_reads(readlist,logo_handle)

def load_sequences_from_flat_file(seq_handle):
    seq_dict = {}
    i = 0
    for curseq in seq_handle:
        i+= 1
        curseq = curseq.strip()
        seq_dict.setdefault(len(curseq),[]).append(curseq)
    print "# Total:", i
    return seq_dict

def load_sequences_from_sam(samfile,region,strand,verbose):
    i = 0
    min_coord = sys.maxint
    max_coord = 0
    chrom, start_b0, end_b0 = parse_region_string(region)
    seq_dict = {}
    for aread in samfile.fetch(region=region):
        if aread.overlap(start_b0,end_b0)==0 or aread.is_unmapped:
            continue
        if (strand=="plus" and aread.is_reverse) or (strand=="minus" and not(aread.is_reverse)):
            continue
        if verbose:
            print aread.seq, aread
        max_coord = max(max_coord,aread.aend)
        min_coord = min(min_coord,aread.pos)
        
        i+= 1
        curseq = aread.seq
        seq_dict.setdefault(len(curseq),[]).append(curseq)
    # print min_coord, max_coord
    print "# Total:", i
    return seq_dict

def generate_logo_from_reads(readlist,logo_handle,show_fineprint=True):
    seq_string = cStringIO.StringIO('\n'.join(readlist))
    seqs = weblogolib.read_seq_data(seq_string)
    data = weblogolib.LogoData.from_seqs(seqs)
    options = weblogolib.LogoOptions(color_scheme=weblogolib.classic,
                                     scale_width=False)
    options.fineprint="count: {0}".format(len(seqs))
    options.show_fineprint=show_fineprint
    # options.title = "A Logo Title"

    logo_format = weblogolib.LogoFormat(data, options)
    pdf_logo = weblogolib.pdf_formatter( data, logo_format)
    logo_handle.write(pdf_logo)
    # weblogolib.pdf_formatter( data, logo_format, logo_handle)

if __name__ == "__main__":
    main()
