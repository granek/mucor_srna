from optparse import OptionParser
import sys
import os
import pysam
import re
import itertools
import functools
from operator import attrgetter

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import generic_dna
from Bio import SeqIO
from Bio.Align import AlignInfo
import Bio

BAM_CMATCH     = 0
BAM_CINS       = 1
BAM_CDEL       = 2
BAM_CREF_SKIP  = 3


region_re = re.compile("([^:]+)(:(\d+)-(\d+))?")

def main():
    usage = 'usage: %prog [-h/--help] SAMFILE REFSEQ REGION\nREGION format is chrom:start-end'
    parser = OptionParser(usage)
    parser.add_option("-f", "--flank",action="store", metavar="FLANK_LEN", type="int", default=0,
                      help="Include FLANK_LEN bases from reference on either side of reads")
    parser.add_option("--maxskip",action="store", type="int", metavar="SKIPLEN",
                      help="Exclude reads with skip lengths larger than SKIPLEN")
    parser.add_option("--duplicates",action="store_true", 
                      help="Combine duplicate reads and include duplicate count in read name")
    parser.add_option("-o", "--outfile",action="store", metavar="FILENAME",
                      help="Output results to FILENAME (default: STDOUT)")
    parser.add_option("--minreads",action="store", type="int", metavar="NUM",
                      help="Only output reads with a count >= NUM")
    parser.add_option("--latex",action="store", metavar="FILENAME", default="-",
                      help="Output latex annotations to FILENAME")
    

    (options, args) = parser.parse_args()

    print >>sys.stderr, args
    if len(args)==3:
        samfile_name = args[0]
        refseq_name = args[1]
        region = args[2]
    else:
        parser.print_help()
        sys.exit(1)

    flank_len = options.flank
    refseq = pysam.Fastafile(refseq_name)

    samfile = pysam.Samfile(samfile_name)
    # print >>sys.stderr, "samfile.filename", samfile.filename

    if options.outfile:
        outhandle = open(options.outfile,"w")
    else:
        outhandle = sys.stdout
    
    chrom, start_b0, end_b0 = parse_region_string(region)
    read_iterator = load_samfile_region(samfile,chrom,start_b0,end_b0)
    outfile_name = samfile.filename.replace(".sam",'_'.join((chrom,str(start_b0+1),str(end_b0)))+".align.fsa")
    
    if options.maxskip:
        maxskip_filter = functools.partial(maxskip_test,maxskip=options.maxskip)
        read_iterator = itertools.ifilter(maxskip_filter, read_iterator)
    if options.duplicates:
        read_iterator,read_count_dict = combine_duplicates(read_iterator)
        # for read in read_iterator:
        #     print read_count_dict[generate_read_key(read)], read
        if options.minreads:
            read_iterator = [read for read in read_iterator if
                             read_count_dict[generate_read_key(read)] >= options.minreads]
            read_count_dict = dict([(readkey,read_count_dict[readkey]) for readkey in
                                map(generate_read_key, read_iterator)])
        # order the read_iterator by read count
        # Starting with Python 2.2, sorts are guaranteed to be stable.
        # That means that when multiple records have the same key, their original order is preserved.
        # This wonderful property lets you build complex sorts in a series of sorting steps.
        # For example, to sort the student data by descending grade and then ascending age, do the age sort first and then sort again using grade
        # read_dict = dict([(generate_read_key(read),read) for read in read_iterator])
        # read_iterator = [read_dict[readkey] for readkey in
        #                  sorted(read_count_dict, key=read_count_dict.get, reverse=True)]
        read_iterator.sort( key=attrgetter("pos", "alen"))
        # read_iterator.sort(key=lambda read: read_count_dict[generate_read_key(read)],reverse=True)
        
    else:
        # read_iterator.sort(key=attrgetter("is_reverse", "pos", "cigar", "qual"))
        for read in sorted(list(read_iterator), key=attrgetter("is_reverse", "pos", "cigar", "qual")):
            print average_quality(read), read
    refname = get_refname(read_iterator,samfile)
    # print "refname:", refname
    for_readlist, rev_readlist = split_strands(read_iterator)
    # print_reads(read_iterator)
    # sys.exit(1)

    
    
    # start = min([read.pos for read in read_iterator]) - flank_len
    # end = max ([read.aend for read in read_iterator]) + flank_len
    align_len = (end_b0-start_b0)


    print "NUM READS: {0}".format(len(read_iterator))

    record_list = []
    refseq = SeqRecord(Seq(refseq.fetch(reference=refname, start=start_b0, end=end_b0), generic_dna), id="ref",description="")
    for i,read in enumerate(for_readlist):
        rec = SeqRecord(Seq(gap_read(read,start_b0,align_len), generic_dna),
                        id="{0}".format(read_count_dict[generate_read_key(read)]),description="")
        #id="(AS){0}".format(read_count_dict[generate_read_key(read)]),description="")
        record_list.append(rec)
    # print ">{0}\n".format(refname), refseq.fetch(reference=refname, start=start_b0, end=end_b0)
    record_list.append(refseq)
    
    for i,read in enumerate(rev_readlist):
        rec = SeqRecord(Seq(gap_read(read,start_b0,align_len), generic_dna),
                        id="{0}".format(read_count_dict[generate_read_key(read)]),description="")
        # id="(S){0}".format(read_count_dict[generate_read_key(read)]),description="")
        record_list.append(rec)
        # print ">rev{0}".format(i+1)
        # print gap_read(read,start)
    samfile.close()
    # SeqIO.write(record_list, options.outfile, "fasta")
    SeqIO.write(record_list, outhandle, "fasta")
    # print outfile_name
    for rec in record_list:
        print rec.seq
    if options.latex:
        generate_latex(options.latex, start_b0+1,end_b0,for_readlist,rev_readlist)

def parse_region_string(region_string):
    region_re = re.compile("([^:]+)(:(\d+)-(\d+))?")
    region_match = region_re.match(region_string)
    if region_match is None:
        raise "Incorrect region format:", region_string
    # print region_match.groups()
    chrom = region_match.group(1)
    if (region_match.group(3) == None) or (region_match.group(4) == None):
        start_b0 = end_b0 = None
    else:
        start_b0 = int(region_match.group(3))-1
        end_b0 = int(region_match.group(4))
    return chrom, start_b0, end_b0
        
def average_quality(read):
    return sum(map(ord, read.seq))/float(read.rlen)

def generate_latex(latex_filename,start_coord,end_coord,for_reads,rev_reads):
    latex_handle = open(latex_filename,'w')
    # \startnumber{ref}{1803667}
    print >>latex_handle, "\startnumber{{ref}}{{{0}}}".format(start_coord)
    print >>latex_handle, "\shaderegion{{ref}}{{{0}..{1}}}{{Red}}{{White}}".format(start_coord,end_coord)


    for i, read in enumerate(rev_reads):
        if len(read.cigar) == 1:
            print >>latex_handle, "\\tintregion{{{0}}}{{1..100}}".format(i+2+len(for_reads))
    # \tintregion{rev27}{1..100}

    
def get_refname(readiter, samfile):
    refname = None
    for read in readiter:
        if refname:
            if refname != samfile.getrname(read.tid):
                raise StandardError, "Read reference names are not the same:", refname != samfile.getrname(read.tid)
        else:
            refname = samfile.getrname(read.tid)
    return refname

def split_strands(readiter):
    for_readlist = []
    rev_readlist = []
    for read in readiter:
        if read.is_reverse:
            rev_readlist.append(read)
        else:
            for_readlist.append(read)
    return for_readlist, rev_readlist


def print_reads(readiter):
    for read in readiter:
        print read.mapq, read.qqual, read.qual, read

def maxskip_test(read,maxskip):
    for operation,length in read.cigar:
        if operation == BAM_CREF_SKIP and length > maxskip:
            return False
    return True

def strict_region_test(read,start,end):
    if read.pos < start:
        return False
    elif end < (read.pos + read.alen):
        return False
    else:
        return True

def generate_read_key(read):
    return "{0.is_reverse:b},{0.pos},{0.cigar}".format(read)

def combine_duplicates_OLD(readiter):
    """count all reads that have the same coordinates (strand, POS, CIGAR), and ?discard all but the one with ?best? quality"""
    read_dict = {}
    read_count_dict = {}
    for read in readiter:
        read_key = generate_read_key(read)
        if (read_key not in read_dict) or (average_quality(read_dict[read_key]) < average_quality(read)):
            read_dict[read_key] = read
        read_count_dict[read_key] = read_count_dict.get(read_key,0) + 1
    return read_dict.values(), read_count_dict

def combine_duplicates(readiter):
    """count all reads that have the same coordinates (strand, POS, CIGAR), and ?discard all but the one with ?best? quality"""
    read_dict = {}
    read_count_dict = {}
    consensus_read_list = []
    for read in readiter:
        read_key = generate_read_key(read)
        read_dict.setdefault(read_key,[]).append(read)
        read_count_dict[read_key] = read_count_dict.get(read_key,0) + 1
    for read_key in read_dict:
        consensus_read = find_consensus_read(read_dict[read_key])
        # print consensus_read.__class__, dir(consensus_read)
        # consensus_read.count = read_count_dict[read_key]
        consensus_read_list.append(consensus_read)
    return consensus_read_list, read_count_dict

def find_consensus_read(readlist):
    alignment = Bio.Align.MultipleSeqAlignment([
        SeqRecord(Seq(read.seq, generic_dna), id=read.qname) for read in readlist])
    summary_align = AlignInfo.SummaryInfo(alignment)
    consensus = summary_align.dumb_consensus(threshold=0.5)
    readlist.sort(key=lambda read: average_quality(read), reverse=True)
    for read in readlist:
        if read.seq == str(consensus):
            # print "FOUND CONSENSUS READ"
            return read
    print >>sys.stderr, "Did not find perfect consensus for these reads:"
    for read in readlist:
        print "{0.seq}\t({0.qname})".format(read)
    print "{0}\t(consensus)\n".format(consensus), "-"*50
    return readlist[0]

def load_samfile_region(samfile,chrom,start_b0,end_b0):
    read_iter = samfile.fetch(chrom,start_b0,end_b0)
    strict_region_filter = functools.partial(strict_region_test,start=start_b0,end=end_b0)
    read_iter = itertools.ifilter(strict_region_filter, read_iter)
    return read_iter

def gap_read(read,start,total_len=0):
    aligned_seq = '-'*(read.pos-start)
    cur_pos = 0
    for operation,length in read.cigar:
        if operation == BAM_CMATCH:
            # print aligned_seq, cur_pos, read.seq[cur_pos:cur_pos+length]
            aligned_seq += read.seq[cur_pos:cur_pos+length]
            cur_pos += length
            # print cur_pos
        elif operation == BAM_CREF_SKIP:
            aligned_seq += '-'*length
        else:
            print "CIGAR OPERATION NOT IMPLEMENTED:", operation
    if total_len >  len(aligned_seq):
        # print aligned_seq
        aligned_seq += "-"*(total_len-len(aligned_seq))
        # print aligned_seq
    return aligned_seq


if __name__ == "__main__":
    main()
