from Bio import SeqIO
# from Bio.SeqRecord import SeqRecord
import argparse
# import sys
# import csv

def main():
    parser = argparse.ArgumentParser(description="Find coordinates of matches to QUERY sequences within SUBJECT")
    parser.add_argument("SUBJECT",type=file,
                         help="FASTA file containing the sequence to search")
    parser.add_argument("QUERY",type=file,
                         help="FASTA file containing the sequences to search for")
    args = parser.parse_args()

    # gff_writer = csv.writer(sys.stdout,delimiter="\t",quoting=csv.QUOTE_MINIMAL)
    
    for cur_query in SeqIO.parse(args.QUERY,"fasta"):
        # print len(cur_query), cur_query.id
        query_seq = cur_query.seq
        query_revcomp = cur_query.reverse_complement().seq
        args.SUBJECT.seek(0)
        for cur_subject in SeqIO.parse(args.SUBJECT,"fasta"):
            run_search(cur_subject,cur_query,False)
            run_search(cur_subject,cur_query,True)
            # subject_seq = cur_subject.seq
            # start = 0
            # hit_coord = -2
            # while (hit_coord != -1) and (start < len(cur_subject)):
            #     hit_coord = subject_seq.find(query_seq, start)
            #     if hit_coord >= 0:
            #         start = hit_coord + 1
            #         transcript_id = cur_query.description.replace(cur_query.id,"").strip()
            #         print "\t".join(map(str,(cur_subject.id, "modified", "3UTR", hit_coord+1, hit_coord+len(cur_query),".","+",".", 'name "{0}"; {1}'.format(cur_query.id,transcript_id))))
            # start = 0
            # hit_coord = -2
            # while (hit_coord != -1) and (start < len(cur_subject)):
            #     hit_coord = subject_seq.find(query_revcomp, start)
            #     if hit_coord >= 0:
            #         start = hit_coord + 1
            #         print cur_query.id, cur_subject.id, hit_coord+1, hit_coord+len(query_revcomp),"-"

def run_search(subject,query,revcomp):
    subject_seq = subject.seq
    if revcomp :
        query_seq = query.reverse_complement().seq
        strand = "-"
    else:
        query_seq = query.seq        
        strand = "+"
    start = 0
    hit_coord = -2
    while (hit_coord != -1) and (start < len(subject)):
        hit_coord = subject_seq.find(query_seq, start)
        if hit_coord >= 0:
            start = hit_coord + 1
            transcript_id = query.description.replace(query.id,"").strip()
            print "\t".join(map(str,(subject.id, "modified", "3UTR", hit_coord+1, hit_coord+len(query),".",strand,".", 'name "{0}"; {1}'.format(query.id,transcript_id))))


if __name__ == "__main__":
    main()
