import csv
import sys
import os
import argparse
import re

processed_re = re.compile(".*lines\s+processed\.")

def main():
    desc = "For all COUNT_FILEs provided, generates a new set of COUNT_FILEs containing the union of features (first column).  Any feature added to a 'harmonized' COUNT_FILE is given a count value of '0'.  Generated COUNT_FILEs have 'union' inserted in their filename before the extension."
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("COUNT_FILE",nargs="+",type=file,help="Input file")
    parser.add_argument("-e", "--extension",metavar="OUTEXT",default=".union.tab",
                        help="Use %(metavar)s as extension of new file (default: %(default)s)")
    # parser.add_argument("-o", "--output",type=argparse.FileType('w'),default=sys.stdout,metavar="OUTFILE")
    args = parser.parse_args()

    # Load files and identify all features
    featureset = set()
    filedict = {}
    dialect = "excel-tab"
    for curfile in args.COUNT_FILE:
        # dialect=csv.Sniffer().sniff(open(curfile).next())
        cur_reader = csv.reader(curfile, dialect=dialect)
        filedict[curfile.name] = {}
        for line in cur_reader:
            if len(line)==1 and processed_re.match(line[0]):
                # print "SKIPPING:", line
                pass
            elif len(line) != 2:
                print "NOT 2 ELEMENTS:", line
            else:
                feature,count = line
                filedict[curfile.name][feature] = int(count)
        featureset.update(filedict[curfile.name].keys())
        
    feature_union = sorted(featureset)
    for curfile in args.COUNT_FILE:
        base,ext = os.path.splitext(curfile.name)
        newfile = base + args.extension
        # dialect=csv.Sniffer().sniff(open(curfile).next())
        outwriter = csv.writer(open(newfile,'w'), dialect=dialect,quoting=csv.QUOTE_NONNUMERIC)
        for feature in feature_union:
            outwriter.writerow((feature, filedict[curfile.name].get(feature,0)))
        
if __name__ == "__main__":
    main()
