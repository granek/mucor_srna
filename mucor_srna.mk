.PHONY: clean

PYTHON=python
SCRIPTS_DIR=scripts
REF_DIR=ref
##--------------------------------------------------------------------------------
GENOME_DIR=genome
GENOME=$(GENOME_DIR)/Mucor_circinelloides_v2_scaffolds.fasta
RAW_GFF=$(GENOME_DIR)/Mucor_circinelloides_v2_filtered_genes.gff
GFF=$(basename $(RAW_GFF)).editted.gff
GENOME_URL := http://genome.jgi.doe.gov//Mucci2/download/Mucor_circinelloides_v2_scaffolds.fasta.gz
GFF_URL := http://genome.jgi.doe.gov/Mucci2/download/Mucor_circinelloides_v2_filtered_genes.gff.gz

RACE_SEQ=$(REF_DIR)/patA_fkbA_3RACE_seq.fasta
RACE_SAM=$(TEMP_DIR)/$(notdir $(basename $(RACE_SEQ))).sam
##--------------------------------------------------------------------------------
FASTQ_DIR=fastqs
BAM_DIR=bamfiles
TRANSCRIPTOME=genome/tophat_mcv2_transcritome
INDEXBASE=$(basename $(GENOME))
INDEX_FILES=$(addprefix $(INDEXBASE),.1.bt2 .2.bt2 .3.bt2 .4.bt2 .rev.1.bt2 .rev.2.bt2)
FIG_DIR=figures
TEMP_DIR=temp
COUNT_DIR=readcounts
SRA_DIR=sra
#------------------------------
EM1_1_NAME=EM1_1_CGATGT
EM1_2_NAME=EM1_2_TTAGGC
EM2_1_NAME=EM2_1_TGACCA
EM3_1_NAME=EM3_1_ACAGTG
EM3_2_NAME=EM3_2_GCCAAT
WT_NAME=WT_ATCACG
SAMPLE_LABELS = $(WT_NAME),$(EM1_1_NAME),$(EM2_1_NAME),$(EM3_1_NAME),$(EM1_2_NAME),$(EM3_2_NAME)
SHORT_SAMPLE_LABELS = WT,EM1_1,EM2_1,EM3_1,EM1_2,EM3_2

#------------------------------
EM1_1_ADAPT=TGGAATTCTCGGGTGCCAAGGAACTCCAGTCACCGATGTATCTCGTATGCCGTCTTCTGCTTG
EM1_2_ADAPT=TGGAATTCTCGGGTGCCAAGGAACTCCAGTCACTTAGGCATCTCGTATGCCGTCTTCTGCTTG
EM2_1_ADAPT=TGGAATTCTCGGGTGCCAAGGAACTCCAGTCACTGACCAATCTCGTATGCCGTCTTCTGCTTG
EM3_1_ADAPT=TGGAATTCTCGGGTGCCAAGGAACTCCAGTCACACAGTGATCTCGTATGCCGTCTTCTGCTTG
EM3_2_ADAPT=TGGAATTCTCGGGTGCCAAGGAACTCCAGTCACGCCAATATCTCGTATGCCGTCTTCTGCTTG
WT_ADAPT=TGGAATTCTCGGGTGCCAAGGAACTCCAGTCACATCACGATCTCGTATGCCGTCTTCTGCTTG
#------------------------------
# FKBA_REGION=scaffold_03:1803135-1803768
FKBA_REGION=scaffold_03:1803250-1803768
#------------------------------
EM1_1_BAM=$(BAM_DIR)/$(EM1_1_NAME)/accepted_hits.bam
EM1_2_BAM=$(BAM_DIR)/$(EM1_2_NAME)/accepted_hits.bam
EM2_1_BAM=$(BAM_DIR)/$(EM2_1_NAME)/accepted_hits.bam
EM3_1_BAM=$(BAM_DIR)/$(EM3_1_NAME)/accepted_hits.bam
EM3_2_BAM=$(BAM_DIR)/$(EM3_2_NAME)/accepted_hits.bam
WT_BAM=$(BAM_DIR)/$(WT_NAME)/accepted_hits.bam

MAPPED_BAMS = $(EM1_1_BAM) $(EM1_2_BAM) $(EM2_1_BAM) $(EM3_1_BAM) $(EM3_2_BAM) $(WT_BAM)
UNMAPPED_BAMS =  $(addsuffix unmapped.bam,$(dir $(MAPPED_BAMS)))
UNMAPPED_BAIS =  $(addsuffix .bai,$(UNMAPPED_BAMS))
## unmapped_indices : $(UNMAPPED_BAIS)
#------------------------------
TRIMMED_FASTQS = $(FASTQ_DIR)/EM1_1_CGATGT_L002_R1_001_3clip.fastq.gz $(FASTQ_DIR)/EM1_2_TTAGGC_L002_R1_001_3clip.fastq.gz $(FASTQ_DIR)/EM2_1_TGACCA_L002_R1_001_3clip.fastq.gz $(FASTQ_DIR)/EM3_1_ACAGTG_L002_R1_001_3clip.fastq.gz $(FASTQ_DIR)/EM3_2_GCCAAT_L002_R1_001_3clip.fastq.gz $(FASTQ_DIR)/WT_ATCACG_L002_R1_001_3clip.fastq.gz 

RAW_FASTQS = $(FASTQ_DIR)/EM1_1_CGATGT_L002_R1_001.fastq.gz $(FASTQ_DIR)/EM1_2_TTAGGC_L002_R1_001.fastq.gz $(FASTQ_DIR)/EM2_1_TGACCA_L002_R1_001.fastq.gz $(FASTQ_DIR)/EM3_1_ACAGTG_L002_R1_001.fastq.gz $(FASTQ_DIR)/EM3_2_GCCAAT_L002_R1_001.fastq.gz $(FASTQ_DIR)/WT_ATCACG_L002_R1_001.fastq.gz 
#------------------------------
TOPHAT_THREADS=12
#------------------------------
PILEUP_SAMPLE_TABLE=$(REF_DIR)/pileup_sample_table.csv

dir_guard=@mkdir -p $(@D)
mv_tmp=@mv $@.TMP $@

# .SECONDARY: $(TRIMMED_FASTQS) $(MAPPED_BAMS)
.SECONDARY:

# make all  -l 16 -j # for parallel make
# mkdir makefile_oldparams; cd makefile_oldparams/ ; ln -s $MUCOR/mucor_manuscript/mucor_srna/Makefile . ; make setup; make trimmed_fastqs -l 12 -j ; make  bams; make figs
# ------------------------------
# make setup; make trimmed_fastqs -l 12 -j ; make  bams; make figs ; make counts --load-average 10 --jobs 8 
#--------------------------------------------------------------------------------
all: seqs figs

todo :
	@echo "Switch back to using own scripts directory"
	@echo "Rscript correlated_gene_analysis.R"

seqs : $(GENOME) $(GFF)
	ls -l $^

figs: $(FIG_DIR)/read_size_distrib__antisense.pdf $(FIG_DIR)/first_base_distrib__antisense.pdf $(FIG_DIR)/fkba_readstack_subplot.pdf $(FIG_DIR)/wt_fkba_logos__antisense.pdf corgene $(FIG_DIR)/fkba_exon_spanning_reads.pdf # $(FIG_DIR)/srna_summary_stats.csv 
# working :  trimmed_fastqs
alt_figs: $(FIG_DIR)/read_size_distrib__sense.pdf $(FIG_DIR)/read_size_distrib__antisense.pdf

stats : $(FIG_DIR)/srna_summary_stats.csv

# fastqs : $(RAW_FASTQS) $(TRIMMED_FASTQS)
# trimmed_fastqs: $(FASTQ_DIR)/EM1_1_CGATGT_L002_R1_001_3clip.fastq.gz $(FASTQ_DIR)/EM1_2_TTAGGC_L002_R1_001_3clip.fastq.gz $(FASTQ_DIR)/EM2_1_TGACCA_L002_R1_001_3clip.fastq.gz $(FASTQ_DIR)/EM3_1_ACAGTG_L002_R1_001_3clip.fastq.gz $(FASTQ_DIR)/EM3_2_GCCAAT_L002_R1_001_3clip.fastq.gz $(FASTQ_DIR)/WT_ATCACG_L002_R1_001_3clip.fastq.gz 

# raw_fastqs: $(FASTQ_DIR)/EM1_1_CGATGT_L002_R1_001.fastq.gz $(FASTQ_DIR)/EM1_2_TTAGGC_L002_R1_001.fastq.gz $(FASTQ_DIR)/EM2_1_TGACCA_L002_R1_001.fastq.gz $(FASTQ_DIR)/EM3_1_ACAGTG_L002_R1_001.fastq.gz $(FASTQ_DIR)/EM3_2_GCCAAT_L002_R1_001.fastq.gz $(FASTQ_DIR)/WT_ATCACG_L002_R1_001.fastq.gz 

bams : $(MAPPED_BAMS)
test: $(TRIMMED_FASTQS) # raw_fastqs trimmed_fastqs bams figs
working: setup test
#********************************************************************************
#********************************************************************************
# THESE ARE TEMPORARY AND SHOULD BE CONTAINED IN repository
# setup: dirs python_scripts other_stuff
# dirs:
# 	mkdir -p $(GENOME_DIR) $(FASTQ_DIR) $(BAM_DIR) $(FIG_DIR) $(SCRIPTS_DIR) $(REF_DIR) $(TEMP_DIR) $(COUNT_DIR)
# python_scripts:
# 	cp -a $(SCRIPTS)/find_branch_site.py $(SCRIPTS_DIR)
# 	cp -a $(SCRIPTS)/generate_alignment_fasta.py $(SCRIPTS_DIR)
# 	cp -a $(SCRIPTS)/trim_srna_reads.py $(SCRIPTS_DIR)
# 	cp -a $(SCRIPTS)/search_for_sequence.py $(SCRIPTS_DIR)
# 	cp -a $(SCRIPTS)/read_size_distribution.py $(SCRIPTS_DIR)
# 	cp -a $(SCRIPTS)/calculate_basecounts.py $(SCRIPTS_DIR)
# 	cp -a $(SCRIPTS)/plot_pileup.py $(SCRIPTS_DIR)
# 	cp -a $(SCRIPTS)/find_3utrs.py $(SCRIPTS_DIR)
# 	cp -a $(SCRIPTS)/weblogos_from_sam.py $(SCRIPTS_DIR)
# other_stuff:
# 	# cp $(MUCOR)/mucor_manuscript/mucor_srna/misc_data/* $(REF_DIR)
# 	cp -a $(MUCOR)/mucor_manuscript/mucor_srna/misc_data/3prime_utrs.fasta $(REF_DIR)
# 	cp -a $(MUCOR)/mucor_manuscript/mucor_srna/misc_data/patA_fkbA_3RACE_seq.fasta $(REF_DIR)
# 	cp -a $(MUCOR)/mucor_manuscript/mucor_srna/figures/fkba_exon_spanning_reads.tex $(TEMP_DIR)
# 	# cp $(MUCOR)/mucor_manuscript/mucor_srna/figures/texshade.sty $(FIG_DIR)
# 	# cp $(MUCOR)/mucor_manuscript/mucor_srna/misc_data/sample_table.csv $(REF_DIR)

$(TEMP_DIR)/fkba_exon_spanning_reads.tex : $(FIG_DIR)/fkba_exon_spanning_reads.tex
	cp -a $< $(TEMP_DIR)
#********************************************************************************
#********************************************************************************
#--------------------------------------------------------------------------------
# Get raw FASTQs
# FASTQ_SOURCE_DIR=/nfs/gems_sata/heitman/mucor_srna/srna_reads
# $(FASTQ_DIR)/%_L002_R1_001.fastq.gz :
# 	cp -a $(FASTQ_SOURCE_DIR)/$(subst $(FASTQ_DIR),,$@) $(FASTQ_DIR)
#--------------------------------------------------------------------------------

#================================================================================
#================================================================================
#==========================
# DOWNLOAD REFERENCE GENOME
#==========================
# Download reference genome
jgi_cookies :
ifndef JGI_USER
	$(info JGI_USER not defined and $(GENOME) does not exist)
	$(info To fix:)
	$(info 1. Manually download $(GENOME_URL), and gunzip it to $(GENOME_DIR))
	$(info 2. Define "JGI_USER=username" in this makefile, where "username" is the username for JGI website)
	$(info 3. Run "export JGI_USER=username", where "username" is the username for JGI website)
	$(error Need $(GENOME))
else ifndef JGI_PASS
	$(info JGI_PASS not defined and $(GENOME) does not exist)
	$(info $(GENOME) does not exist)
	$(info To fix:)
	$(info 1. Manually download $(GENOME_URL), and gunzip it to $(GENOME_DIR))
	$(info 2. Define "JGI_PASS=password" in this makefile, where "password" is the password for JGI website)
	$(info 3. Run "export JGI_PASS=password", where "password" is the password for JGI website)
	$(error Need $(GENOME))
else
	curl https://signon.jgi.doe.gov/signon/create --data-ascii login=$(JGI_USER)\&password=$(JGI_PASS) -b $@ -c $@ > /dev/null
endif

$(GENOME) : jgi_cookies
	$(dir_guard)
	curl -o $@.TMP.gz $(GENOME_URL) -b $^ # -c $^
	gunzip $@.TMP.gz
	# following corrects FASTA formating that bothers samtools faidx
	# $(PYTHON) $(SCRIPTS)/reformat_fasta.py $(GENOME).TMP -o $(GENOME).REFORMAT
	$(mv_tmp)

#--------------------------------------------------------------------------------
# Download and GFF from JGI and add 3' UTRs
# alternative_3utr_mapping : $(REF_DIR)/patA_fkbA_3RACE_seq.fasta $(INDEX_FILES) other_stuff python_scripts
$(RAW_GFF) : jgi_cookies
	$(dir_guard)
	curl -o $@.TMP.gz $(GFF_URL) -b $< # -c $<
	gunzip $@.TMP.gz
	$(mv_tmp)

$(RACE_SAM) : $(RACE_SEQ) $(INDEX_FILES) 
	$(dir_guard)
	bowtie2 -f --local -x $(basename $(basename $(word 2,$^))) -U $< -S $@.TMP
	$(mv_tmp)

$(GFF) : $(RAW_GFF) $(RACE_SAM) 
	$(dir_guard)
	cp -a $< $@.TMP
	$(PYTHON) $(SCRIPTS_DIR)/find_3utrs.py $(word 2,$^) --gff $< >>  $@.TMP
	$(mv_tmp)
#================================================================================
#================================================================================
#==========================
# DOWNLOAD READS
#==========================
srafiles : $(SRA_DIR)/SRR1209506.sra $(SRA_DIR)/SRR1209507.sra $(SRA_DIR)/SRR1209508.sra $(SRA_DIR)/SRR1209509.sra $(SRA_DIR)/SRR1209510.sra $(SRA_DIR)/SRR1209511.sra

$(SRA_DIR)/%.sra :
	wget -r -nd -P $(@D) -A sra ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByStudy/sra/SRP/SRP040/SRP040724/$*/$*.sra

#==========
# PREP DATA
#==========
#--------------------------------------------------------------------------------
# # Generate trimmed FASTQs from raw FASTQs
$(FASTQ_DIR)/$(WT_NAME)_L002_R1_001_3clip.fastq.gz : $(SRA_DIR)/SRR1209506.sra
	$(dir_guard)
	fastq-dump $< --stdout | fastx_clipper -n  -z  -c  -Q33  -a $(WT_ADAPT) -o $@.TMP
	mv $@.TMP $@

$(FASTQ_DIR)/$(EM1_1_NAME)_L002_R1_001_3clip.fastq.gz : $(SRA_DIR)/SRR1209507.sra
	$(dir_guard)
	fastq-dump $< --stdout | fastx_clipper -n  -z  -c  -Q33  -a $(EM1_1_ADAPT) -o $@.TMP
	mv $@.TMP $@

$(FASTQ_DIR)/$(EM2_1_NAME)_L002_R1_001_3clip.fastq.gz : $(SRA_DIR)/SRR1209508.sra
	$(dir_guard)
	fastq-dump $< --stdout | fastx_clipper -n  -z  -c  -Q33  -a $(EM2_1_ADAPT) -o $@.TMP
	mv $@.TMP $@

$(FASTQ_DIR)/$(EM3_1_NAME)_L002_R1_001_3clip.fastq.gz : $(SRA_DIR)/SRR1209509.sra
	$(dir_guard)
	fastq-dump $< --stdout | fastx_clipper -n  -z  -c  -Q33  -a $(EM3_1_ADAPT) -o $@.TMP
	mv $@.TMP $@

$(FASTQ_DIR)/$(EM1_2_NAME)_L002_R1_001_3clip.fastq.gz : $(SRA_DIR)/SRR1209510.sra
	$(dir_guard)
	fastq-dump $< --stdout | fastx_clipper -n  -z  -c  -Q33  -a $(EM1_2_ADAPT) -o $@.TMP
	mv $@.TMP $@


$(FASTQ_DIR)/$(EM3_2_NAME)_L002_R1_001_3clip.fastq.gz : $(SRA_DIR)/SRR1209511.sra
	$(dir_guard)
	fastq-dump $< --stdout | fastx_clipper -n  -z  -c  -Q33  -a $(EM3_2_ADAPT) -o $@.TMP
	mv $@.TMP $@


#------------------------------------
# $(FASTQ_DIR)/$(EM1_1_NAME)_L002_R1_001_3clip.fastq.gz : $(FASTQ_DIR)/$(EM1_1_NAME)_L002_R1_001.fastq.gz
# 	zcat $< | fastx_clipper -n  -z  -c  -Q33  -a $(EM1_1_ADAPT) -o $@

# $(FASTQ_DIR)/$(EM1_2_NAME)_L002_R1_001_3clip.fastq.gz : $(FASTQ_DIR)/$(EM1_2_NAME)_L002_R1_001.fastq.gz
# 	zcat $< | fastx_clipper -n  -z  -c  -Q33  -a $(EM1_2_ADAPT) -o $@

# $(FASTQ_DIR)/$(EM2_1_NAME)_L002_R1_001_3clip.fastq.gz : $(FASTQ_DIR)/$(EM2_1_NAME)_L002_R1_001.fastq.gz
# 	zcat $< | fastx_clipper -n  -z  -c  -Q33  -a $(EM2_1_ADAPT) -o $@

# $(FASTQ_DIR)/$(EM3_1_NAME)_L002_R1_001_3clip.fastq.gz : $(FASTQ_DIR)/$(EM3_1_NAME)_L002_R1_001.fastq.gz
# 	zcat $< | fastx_clipper -n  -z  -c  -Q33  -a $(EM3_1_ADAPT) -o $@

# $(FASTQ_DIR)/$(EM3_2_NAME)_L002_R1_001_3clip.fastq.gz : $(FASTQ_DIR)/$(EM3_2_NAME)_L002_R1_001.fastq.gz
# 	zcat $< | fastx_clipper -n  -z  -c  -Q33  -a $(EM3_2_ADAPT) -o $@

# $(FASTQ_DIR)/$(WT_NAME)_L002_R1_001_3clip.fastq.gz : $(FASTQ_DIR)/$(WT_NAME)_L002_R1_001.fastq.gz
# 	zcat $< | fastx_clipper -n  -z  -c  -Q33  -a $(WT_ADAPT) -o $@

# $(FASTQ_DIR)/%_3clip.fastq.gz : $(FASTQ_DIR)/%.fastq.gz
# 	$(PYTHON) $(SCRIPTS_DIR)/trim_srna_reads.py $<
# 	# RNA 3’ Adapter (RA3) + part of TruSeq Adapter : TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC
# 	# zcat $< | fastx_clipper -n  -z  -c  -Q33  -a TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC -o $@
# 	# RNA 3’ Adapter (RA3): TGGAATTCTCGGGTGCCAAGG
# 	# zcat $< | fastx_clipper -n  -z  -c  -Q33  -a TGGAATTCTCGGGTGCCAAGG -o $@
#--------------------------------------------------------------------------------
# Index reference genome
%.fa : %.fasta
	ln -s $(notdir $<) $@

%.1.bt2 %.2.bt2 %.3.bt2 %.4.bt2 %.rev.1.bt2 %.rev.2.bt2 : %.fa
	bowtie2-build $< $(basename $(basename $@))
	# ln -s $(notdir $<) $(basename $(basename $@)).fa
#--------------------------------------------------------------------------------

##===============================================================================
##===============================================================================
#==========
# MAP READS
#==========
#--------------------------------------------------------------------------------
# Map trimmed reads to reference genome
# $(BAM_DIR)/%/accepted_hits.bam : $(FASTQ_DIR)/%_L002_R1_001_3clip.fastq.gz
# $(BAM_DIR)/%/accepted_hits.bam : $(FASTQ_DIR)/%_L002_R1_001_3clip.fastq.gz $(GFF) $(INDEXBASE).1.bt2 $(INDEXBASE).2.bt2 $(INDEXBASE).3.bt2 $(INDEXBASE).4.bt2 $(INDEXBASE).rev.1.bt2 $(INDEXBASE).rev.2.bt2
$(BAM_DIR)/%/accepted_hits.bam : $(FASTQ_DIR)/%_L002_R1_001_3clip.fastq.gz $(GFF) $(INDEX_FILES)
	$(eval OUTDIR := $(subst accepted_hits.bam,,$@))
	echo $(OUTDIR)
	mkdir -p $(OUTDIR)
	# New parameters
	# tophat2 -p $(TOPHAT_THREADS) -G $(GFF) --no-novel-juncs --library-type=fr-secondstrand --output-dir=$(OUTDIR)  --transcriptome-index=$(TRANSCRIPTOME) --no-coverage-search --transcriptome-max-hits=1 --prefilter-multihits --max-intron-length=500 --min-intron-length=30 --max-multihits=1 $(INDEXBASE) $<
	# Following is OLD parameters
	tophat2 -p $(TOPHAT_THREADS) -G $(GFF) --no-novel-juncs --library-type=fr-secondstrand --output-dir=$(OUTDIR)  --transcriptome-index=$(TRANSCRIPTOME) --no-coverage-search --max-intron-length=4000 --min-intron-length=30 --max-multihits=1 --min-segment-intron=30 --read-realign-edit-dist=0 $(INDEXBASE) $<
	#                      
#--------------------------------------------------------------------------------
# Index BAMs
%.bam.bai : %.bam
	samtools index $^
#--------------------------------------------------------------------------------
# final_bams: $(BAM_DIR)/EM1_1_CGATGT/accepted_hits.bam $(BAM_DIR)/EM1_2_TTAGGC/accepted_hits.bam $(BAM_DIR)/EM2_1_TGACCA/accepted_hits.bam $(BAM_DIR)/EM3_1_ACAGTG/accepted_hits.bam $(BAM_DIR)/EM3_2_GCCAAT/accepted_hits.bam $(BAM_DIR)/WT_ATCACG/accepted_hits.bam

# $(BAM_DIR)/%.bam : $(FASTQ_DIR)/%_L002_R1_001_3clip.fastq.gz $(GFF) index_files
# # $(BAM_DIR)/%.bam : $(FASTQ_DIR)/%_L002_R1_001_3clip.fastq.gz
# # $(BAM_DIR)/%.bam :
# 	$(eval OUTDIR := $(subst .bam,,$@))
# 	echo $(OUTDIR)
# 	mkdir -p $(OUTDIR)
# 	tophat2 -p 12 -G $(GFF) --no-novel-juncs --library-type=fr-secondstrand --output-dir=$(OUTDIR)  --transcriptome-index=$(TRANSCRIPTOME) --no-coverage-search --transcriptome-max-hits=1 --prefilter-multihits --max-intron-length=500 --min-intron-length=30 --max-multihits=1 $(INDEXBASE) $<
# 	ln -s $(OUTDIR)/accepted_hits.bam $@

# $(BAM_DIR)/%.bam : $(BAM_DIR)/%/accepted_hits.bam
# 	# $(eval OUTDIR := $(subst .bam,,$@))
# 	# echo $(OUTDIR)
# 	# mkdir -p $(OUTDIR)
# 	# tophat2 -p 12 -G $(GFF) --no-novel-juncs --library-type=fr-secondstrand --output-dir=$(OUTDIR)  --transcriptome-index=$(TRANSCRIPTOME) --no-coverage-search --transcriptome-max-hits=1 --prefilter-multihits --max-intron-length=500 --min-intron-length=30 --max-multihits=1 $(INDEXBASE) $<
# 	ln -s $< $@
##===============================================================================
##===============================================================================
ANTISENSE_COUNTS = $(COUNT_DIR)/$(EM1_1_NAME)_anti_counts.tab $(COUNT_DIR)/$(EM1_2_NAME)_anti_counts.tab $(COUNT_DIR)/$(EM2_1_NAME)_anti_counts.tab $(COUNT_DIR)/$(EM3_1_NAME)_anti_counts.tab $(COUNT_DIR)/$(EM3_2_NAME)_anti_counts.tab $(COUNT_DIR)/$(WT_NAME)_anti_counts.tab

SENSE_COUNTS = $(COUNT_DIR)/$(EM1_1_NAME)_sense_counts.tab $(COUNT_DIR)/$(EM1_2_NAME)_sense_counts.tab $(COUNT_DIR)/$(EM2_1_NAME)_sense_counts.tab $(COUNT_DIR)/$(EM3_1_NAME)_sense_counts.tab $(COUNT_DIR)/$(EM3_2_NAME)_sense_counts.tab $(COUNT_DIR)/$(WT_NAME)_sense_counts.tab 

BOTH_STRAND_COUNTS = $(COUNT_DIR)/$(EM1_1_NAME)_both_counts.tab $(COUNT_DIR)/$(EM1_2_NAME)_both_counts.tab $(COUNT_DIR)/$(EM2_1_NAME)_both_counts.tab $(COUNT_DIR)/$(EM3_1_NAME)_both_counts.tab $(COUNT_DIR)/$(EM3_2_NAME)_both_counts.tab $(COUNT_DIR)/$(WT_NAME)_both_counts.tab 

counts : $(ANTISENSE_COUNTS) $(SENSE_COUNTS) $(BOTH_STRAND_COUNTS)
#=============
# COUNT READS
#=============
# $(COUNT_DIR)/%_anti_counts.tab : $(BAM_DIR)/%/accepted_hits.bam $(GFF) $(SCRIPTS_DIR)/harmonize_featuresets.py
# 	$(dir_guard)
# 	samtools view $< | htseq-count --stranded=reverse --quiet --type=exon --idattr=name - $(word 2,$^) 1> $(basename $@).unquoted 2> $@.err
# 	$(PYTHON) $(word 3,$^) $(basename $@).unquoted --extension .tab.tmp
# 	mv $@.tmp $@

# $(COUNT_DIR)/%_sense_counts.tab : $(BAM_DIR)/%/accepted_hits.bam $(GFF) $(SCRIPTS_DIR)/harmonize_featuresets.py
# 	$(dir_guard)
# 	samtools view $< | htseq-count --stranded=yes --quiet --type=exon --idattr=name - $(word 2,$^) 1> $(basename $@).unquoted 2> $@.err
# 	$(PYTHON) $(word 3,$^) $(basename $@).unquoted --extension .tab.tmp
# 	mv $@.tmp $@

# $(COUNT_DIR)/%_both_counts.tab : $(BAM_DIR)/%/accepted_hits.bam $(GFF) $(SCRIPTS_DIR)/harmonize_featuresets.py
# 	$(dir_guard)
# 	samtools view $< | htseq-count --stranded=no --quiet --type=exon --idattr=name - $(word 2,$^) 1> $(basename $@).unquoted 2> $@.err
# 	$(PYTHON) $(word 3,$^) $(basename $@).unquoted --extension .tab.tmp
# 	mv $@.tmp $@
$(COUNT_DIR)/%_anti_counts.unquoted : $(BAM_DIR)/%/accepted_hits.bam $(GFF)
	$(dir_guard)
	samtools view $< | htseq-count --stranded=reverse --quiet --type=exon --idattr=name - $(word 2,$^) 1> $@.tmp 2> $(basename $@).err
	mv $@.tmp $@

$(COUNT_DIR)/%_sense_counts.unquoted : $(BAM_DIR)/%/accepted_hits.bam $(GFF)
	$(dir_guard)
	samtools view $< | htseq-count --stranded=yes --quiet --type=exon --idattr=name - $(word 2,$^) 1> $@.tmp 2> $(basename $@).err
	mv $@.tmp $@

$(COUNT_DIR)/%_both_counts.unquoted : $(BAM_DIR)/%/accepted_hits.bam $(GFF)
	$(dir_guard)
	samtools view $< | htseq-count --stranded=no --quiet --type=exon --idattr=name - $(word 2,$^) 1> $@.tmp 2> $(basename $@).err
	mv $@.tmp $@

$(COUNT_DIR)/%.tab : $(COUNT_DIR)/%.unquoted $(SCRIPTS_DIR)/harmonize_featuresets.py
	$(dir_guard)
	$(PYTHON) $(word 2,$^) $< --extension .tab.tmp
	mv $@.tmp $@

#=============
# MAKE FIGURES
#=============
#--------------------------------------------------------------------------------
AUTO_MSA=$(TEMP_DIR)/AUTO__fkba_exon_spanning_reads.fsa
AUTO_COORDS=$(TEMP_DIR)/AUTO__fkba_exon_spanning_reads_coords.tex

$(AUTO_COORDS) $(AUTO_MSA): $(SCRIPTS_DIR)/generate_alignment_fasta.py $(GENOME) $(EM1_1_BAM) $(EM1_1_BAM).bai
	$(PYTHON) $(SCRIPTS_DIR)/generate_alignment_fasta.py $(EM1_1_BAM) $(GENOME) scaffold_03:1803668-1803774 --duplicates --minreads 5 --latex $(AUTO_COORDS) -o $(AUTO_MSA)

branch_site: $(SCRIPTS_DIR)/find_branch_site.py $(GENOME)
	$(PYTHON) $(SCRIPTS_DIR)/find_branch_site.py scaffold_03:1803668-1803768 $(GENOME)

$(TEMP_DIR)/texshade.dtx :
	$(dir_guard)
	cd $(TEMP_DIR); curl -L -O http://mirrors.ctan.org/macros/latex/contrib/texshade/texshade.dtx

$(TEMP_DIR)/texshade.ins : $(TEMP_DIR)/texshade.dtx
	$(dir_guard)
	cd $(TEMP_DIR); curl -L -O http://mirrors.ctan.org/macros/latex/contrib/texshade/texshade.ins

$(TEMP_DIR)/texshade.sty: $(TEMP_DIR)/texshade.ins
	$(dir_guard)
	cd $(TEMP_DIR) ; latex $(subst $(TEMP_DIR),.,$<)

$(FIG_DIR)/fkba_exon_spanning_reads.pdf: $(TEMP_DIR)/fkba_exon_spanning_reads.tex $(AUTO_COORDS) $(AUTO_MSA) $(TEMP_DIR)/texshade.sty
	$(dir_guard)
	cd $(TEMP_DIR) ; pdflatex $(subst $(TEMP_DIR),.,$<)
	mv $(subst $(FIG_DIR),$(TEMP_DIR),$@) $(FIG_DIR)
#--------------------------------------------------------------------------------
$(FIG_DIR)/read_size_distrib__both.pdf : $(EM1_1_BAM) $(EM1_1_BAM).bai
	$(PYTHON)  $(SCRIPTS_DIR)/read_size_distribution.py $(FKBA_REGION) $(EM1_1_BAM) --barplot $@ both --negative --scale 0.440

$(FIG_DIR)/read_size_distrib__sense.pdf : $(EM1_1_BAM) $(EM1_1_BAM).bai
	$(PYTHON)  $(SCRIPTS_DIR)/read_size_distribution.py $(FKBA_REGION) $(EM1_1_BAM) --barplot $@ sense --negative --scale 0.440

$(FIG_DIR)/read_size_distrib__antisense.pdf : $(EM1_1_BAM) $(EM1_1_BAM).bai
	$(PYTHON)  $(SCRIPTS_DIR)/read_size_distribution.py $(FKBA_REGION) $(EM1_1_BAM) --barplot $@ antisense --negative --scale 0.440
#--------------------------------------------------------------------------------
$(FIG_DIR)/first_base_distrib__antisense.pdf : $(EM1_1_BAM) $(EM1_1_BAM).bai
	$(PYTHON) $(SCRIPTS_DIR)/calculate_basecounts.py $(FKBA_REGION) $(EM1_1_BAM) --scale 0.20 --plot $@
#--------------------------------------------------------------------------------
$(PILEUP_SAMPLE_TABLE) :
	echo WT,$(WT_BAM),\"0, 0, 0\" > $(PILEUP_SAMPLE_TABLE).tmp
	echo EM1-R,$(EM1_1_BAM),\"0, 0, 255\" >> $(PILEUP_SAMPLE_TABLE).tmp
	echo EM1-S,$(EM1_2_BAM),\"0, 128, 200\" >> $(PILEUP_SAMPLE_TABLE).tmp
	echo EM2-R,$(EM2_1_BAM),\"51, 160, 44\" >> $(PILEUP_SAMPLE_TABLE).tmp
	echo EM3-R,$(EM3_1_BAM),\"255, 127, 0\" >> $(PILEUP_SAMPLE_TABLE).tmp
	echo EM3-S,$(EM3_2_BAM),\"208, 64, 32\" >> $(PILEUP_SAMPLE_TABLE).tmp
	mv $(PILEUP_SAMPLE_TABLE).tmp $(PILEUP_SAMPLE_TABLE)
#--------------------------------------------------------------------------------
$(FIG_DIR)/fkba_readstack_subplot.pdf : $(GFF) $(EM1_1_BAM) $(EM1_2_BAM) $(EM2_1_BAM) $(EM3_1_BAM) $(EM3_2_BAM) $(WT_BAM) $(EM1_1_BAM).bai $(EM1_2_BAM).bai $(EM2_1_BAM).bai $(EM3_1_BAM).bai $(EM3_2_BAM).bai $(WT_BAM).bai $(PILEUP_SAMPLE_TABLE) 
	# $(PYTHON) $(SCRIPTS_DIR)/plot_pileup.py --gff  $< --linewidth 3 --legendsize 12 --noxlabel --gene Genemark1.4277_g  --subplot --table $(PILEUP_SAMPLE_TABLE) -o $@
	$(PYTHON) $(SCRIPTS_DIR)/plot_pileup.py --gff  $< --linewidth 3 --legendsize 12 --noxlabel --region scaffold_03:1803135-1803800 --subplot --table $(PILEUP_SAMPLE_TABLE) -o $@

#--------------------------------------------------------------------------------
# $(FIG_DIR)/wt_size_distrib__antisense.csv : $(WT_BAM) $(WT_BAM).bai
# 	$(PYTHON)  $(SCRIPTS_DIR)/read_size_distribution.py $(FKBA_REGION) $< --raw --negative --csv $@
$(FIG_DIR)/wt_fkba_logos__antisense.pdf : $(WT_BAM) $(WT_BAM).bai
	# $(PYTHON) /Users/josh/Documents/BioinfCollabs/$(SCRIPTS_DIR)/weblogos_from_sam.py ../tophatout/WT_ATCACG_L002_R1_001/accepted_hits.bam scaffold_03:1803135-1803768 --outdir /tmp/wt_logos --strand plus
	$(PYTHON) $(SCRIPTS_DIR)/weblogos_from_sam.py $< $(FKBA_REGION) --outdir $(TEMP_DIR) --outroot $(WT_NAME) --strand plus
	cat temp/WT_ATCACG_logolist.txt | convert -density 300x300 -compress lzw -append @-  $(subst $(FIG_DIR),$(TEMP_DIR),$@)
	# convert -density 300x300 -compress lzw -append @$(TEMP_DIR)/$(WT_NAME)_logolist.txt  $(subst $(FIG_DIR),$(TEMP_DIR),$@)
	mv $(subst $(FIG_DIR),$(TEMP_DIR),$@) $@
	# convert -density 300x300 -compress lzw -append @/tmp/wt_logos/accepted_hits_logolist.txt  /tmp/wt_logos/combined.pdf
	# $(PYTHON)  $(SCRIPTS_DIR)/read_size_distribution.py $(FKBA_REGION) $< --raw --negative --csv $@

#=========================
# MAKE Summary Stats Table
#=========================
$(FIG_DIR)/srna_summary_stats.csv : $(MAPPED_BAMS) $(UNMAPPED_BAMS) $(UNMAPPED_BAIS) $(TRIMMED_FASTQS) # $(RAW_FASTQS)
	$(PYTHON) $(SCRIPTS_DIR)/summary_statistics.py --filtered 3clip --samples $(SHORT_SAMPLE_LABELS) --outfile $@ $^

##===============================================================================
##===============================================================================

$(FIG_DIR)/50_shared_axis_%.pdf $(FIG_DIR)/50_nofkba_%.pdf : $(ANTISENSE_COUNTS)
	$(dir_guard)
	Rscript --no-restore correlated_gene_analysis.R --usecwd
	# printf '\a';printf '\a';printf '\a';printf '\a';printf '\a';printf '\a'

corgene : $(FIG_DIR)/50_shared_axis_correlated_genes.pdf $(FIG_DIR)/50_nofkba_correlated_genes.pdf

#=========
# CLEANUP
#=========
clean:
	# rm -f $(FIG_DIR)/AUTO__* $(FIG_DIR)/*.aux $(FIG_DIR)/*.log $(TEMP_DIR)/*
	rm -rf $(TEMP_DIR) jgi_cookies $(FIG_DIR)/*.pdf

reset: clean
	rm -rf $(GENOME_DIR) $(TEMP_DIR)

##================================================================================
##================================================================================
##================================================================================
#--------------------------------------------------------------------------------
FASTQ_LONG=fastqs_long

trimmed_fastqs_long: $(FASTQ_LONG)/EM1_1_CGATGT_L002_R1_001_3clip.fastq.gz $(FASTQ_LONG)/EM1_2_TTAGGC_L002_R1_001_3clip.fastq.gz $(FASTQ_LONG)/EM3_1_ACAGTG_L002_R1_001_3clip.fastq.gz $(FASTQ_LONG)/EM3_2_GCCAAT_L002_R1_001_3clip.fastq.gz $(FASTQ_LONG)/WT_ATCACG_L002_R1_001_3clip.fastq.gz

$(FASTQ_LONG)/%_3clip.fastq.gz : $(FASTQ_LONG)/%.fastq.gz
	# $(PYTHON) $(SCRIPTS_DIR)/trim_srna_reads.py $<
	# RNA 3’ Adapter (RA3) + part of TruSeq Adapter : TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC
	zcat $< | fastx_clipper -n  -z  -c  -Q33  -a TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC -o $@
	# RNA 3’ Adapter (RA3): TGGAATTCTCGGGTGCCAAGG
	# zcat $< | fastx_clipper -n  -z  -c  -Q33  -a TGGAATTCTCGGGTGCCAAGG -o $@
#--------------------------------------------------------------------------------
FASTQ_SHORT=fastqs_short

trimmed_fastqs_short: $(FASTQ_SHORT)/EM1_1_CGATGT_L002_R1_001_3clip.fastq.gz $(FASTQ_SHORT)/EM1_2_TTAGGC_L002_R1_001_3clip.fastq.gz $(FASTQ_SHORT)/EM3_1_ACAGTG_L002_R1_001_3clip.fastq.gz $(FASTQ_SHORT)/EM3_2_GCCAAT_L002_R1_001_3clip.fastq.gz $(FASTQ_SHORT)/WT_ATCACG_L002_R1_001_3clip.fastq.gz

$(FASTQ_SHORT)/%_3clip.fastq.gz : $(FASTQ_SHORT)/%.fastq.gz
	# $(PYTHON) $(SCRIPTS_DIR)/trim_srna_reads.py $<
	# RNA 3’ Adapter (RA3) + part of TruSeq Adapter : TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC
	# zcat $< | fastx_clipper -n  -z  -c  -Q33  -a TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC -o $@
	# RNA 3’ Adapter (RA3): TGGAATTCTCGGGTGCCAAGG
	zcat $< | fastx_clipper -n  -z  -c  -Q33  -a TGGAATTCTCGGGTGCCAAGG -o $@
#--------------------------------------------------------------------------------
# make fastqs_long/EM2_1_TGACCA_L002_R1_001_3clip.fastq.gz ; zdiff fastqs_long/EM2_1_TGACCA_L002_R1_001_3clip.fastq.gz /nfs/gems_sata/heitman/mucor_srna/srna_reads/EM2_1_TGACCA_L002_R1_001_3clip.fastq.gz > fastqs_long/EM2_1_TGACCA_L002_R1_001.diff

# make fastqs/EM2_1_TGACCA_L002_R1_001_3clip.fastq.gz ; zdiff fastqs/EM2_1_TGACCA_L002_R1_001_3clip.fastq.gz /nfs/gems_sata/heitman/mucor_srna/srna_reads/EM2_1_TGACCA_L002_R1_001_3clip.fastq.gz > fastqs/EM2_1_TGACCA_L002_R1_001.diff
#--------------------------------------------------------------------------------
#--------------------------------------------------------------------------------

X=$(FASTQ_DIR)/EM1_1_CGATGT_L002_R1_001.fastq.gz
Y=$(FASTQ_DIR)/WT_ATCACG_L002_R1_001.fastq.gz
barcode:
	echo $(X)
	echo $(Y)
	echo $(subst _L002_R1_001.fastq.gz,,$(X))
	echo $(subst _L002_R1_001.fastq.gz,,$(Y))
	echo $(call substr,$(X),1,10)
#--------------------------------------------------------------------------------
